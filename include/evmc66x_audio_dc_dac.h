/*
 * Copyright (c) 2015 - 2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  \file      evmc66x_audio_dc_dac.h
 *
 *  \brief     Audio DAC low level library header file.
 *
 *  This file contains PCM169x DAC specific structure, typedefs and
 *  function prototypes.
 *
 */

#ifndef _EVMC66X_AUDIO_DC_AUD_DAC_H_
#define _EVMC66X_AUDIO_DC_AUD_DAC_H_

#ifdef __cplusplus
extern "C" {
#endif

/** Register Definitions */
#define PCM169x_RESET_MODE_CTRL	(0x40)
#define PCM169x_AUDIO_FMT	    (0x41)
#define PCM169x_OPEDA_CTRL	    (0x42)
#define PCM169x_REVDA_SEL	    (0x43)
#define PCM169x_SOFT_MUTE	    (0x44)
#define PCM169x_ZERO_FLAG	    (0x45)
#define PCM169x_DEMPH_CTRL	    (0x46)

/** Macro to calculate the Volume Register Offsets */
#define PCM169x_ATT_CONTROL(X)  (72 + X)

/**************************************************************************
 **                       Macro Definitions
 **************************************************************************/

/** Macro for I2C Port Number */
#define PCM169x_I2C_PORT_NUM (I2C_PORT_1)

/** Macro to mute DAC channels */
#define PCM169x_SOFT_MUTE_ALL (0xFF)

/** Macro to Reset the codec */
#define PCM169x_SRST_CTRL_VAL (~(1 << 6))

/** Mask for AMUTE field in the DAC register */
#define PCM169x_AMUTE_MASK  (0x3C)
/** Shift for AMUTE field in the DAC register */
#define PCM169x_AMUTE_SHIFT (2)

/** Macros for Start and End Register Address */
#define PCM169x_REG_START (0x40)
#define PCM169x_REG_END   (0x4F)

/** Macro to enable DEBUG mode */
#define DEBUG_PCM169x_ENABLE

/** Macro to enable register echo in aud_pcm169x_write_reg()
    When this macro is enabled, every register written is read back and
    its value is displayed by aud_pcm169x_write_reg()*/
#define ENABLE_DAC_REG_ECHO (1)

/** DAC API return code for success */
#define AUD_DAC_RET_OK          (0)
/** DAC API return code for generic error */
#define AUD_DAC_RET_FAIL        (-1)
/** DAC API return code for invalid parameter error */
#define AUD_DAC_RET_INV_PARAM   (-2)
/** DAC API return code for I2C access error */
#define AUD_DAC_RET_I2C_ERR     (-3)

/** DAC I2C delay value (usecs) */
#define AUD_DAC_I2C_DELAY     (100)

#ifdef DEBUG_PCM169x_ENABLE
#define DBG_PCM169x(x) IFPRINT(x)
#else
#define DBG_PCM169x(x)
#endif

/**
 * \brief Structure to store register default values.
 *
 */
typedef struct _AudDacRegDefConfig
{
	Uint8 reg;  /**< DAC register address */
	Uint8 def;  /**< DAC register value */

} AudDacRegDefConfig;

/** DAC API return type */
typedef Int32 AUD_DAC_RET;

/**************************************************************************
 **                      API function Prototypes
**************************************************************************/

/**
 * \brief     Configures all the DAC registers to default value.
 *
 * \param     addr [IN] DAC HW instance I2C slave address.
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xDacInit (Uint8 addr);

/**
 * \brief     Resets the PCM169x Codec
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xReset(Uint8 addr);

/**
 * \brief     Sets the DAC Volume
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     vol      [IN] Volume in percentage; 0 to 100
 *
 * \param     chanId   [IN] Channel ID to set volume
 *                          0 to 7 for individual channel volume configuration
 *                          0xF for volume configuration all the 8 channels
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xSetVolume(Uint8 addr, Uint8 vol, Uint8 chanId);

/**
 * \brief     Unmute or Mute DAC Outputs.
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     chanId   [IN] Channel ID offset for mute control
 *                          0x01 - DAC1
 *                          0x02 - DAC2
 *                          0x04 - DAC3
 *                          0x08 - DAC4
 *                          0x10 - DAC5
 *                          0x20 - DAC6
 *                          0x40 - DAC7
 *                          0x80 - DAC8
 *                          0xFF - All channels
 *
 * \param     mute     [IN] Mute configuration control
 *                          1 : Mute DAC Output.
 *                          0 : Unmute DAC Output.
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xSoftMuteCtrl(Uint8 addr, Uint8 chanId, Uint8 mute);

/**
 * \brief     Configures output phase
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     chanId   [IN] Channel ID offset for phase control
 *                          0x01 - DAC1
 *                          0x02 - DAC2
 *                          0x04 - DAC3
 *                          0x08 - DAC4
 *                          0x10 - DAC5
 *                          0x20 - DAC6
 *                          0x40 - DAC7
 *                          0x80 - DAC8
 *                          0xFF - All channels
 *
 * \param     phase    [IN] DAC output phase
 *                          0 - Normal Output.
 *                          1 - Inverted Output.
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xSetOutputPhase(Uint8 addr, Uint8 chanId, Uint8 phase);

/**
 * \brief     Configures the power-save mode
 *
 * \param     addr  [IN] DAC HW instance I2C slave address.
 *
 * \param     mode  [IN] Power mode configuration
 *                       0 - Enable power-save mode
 *                       1 - Disable power-save mode
 *
 * \return    0 if success.
 *
 **/
AUD_DAC_RET aud_pcm169xSetPowerMode(Uint8 addr, Uint8 mode);

/**
 * \brief     Configures the data format and slot width
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     dataFmt    Data type for the codec operation
 *
 *    AUD_DAC_DATA_FORMAT_I2S - 16-/20-/24-/32-bit I2S format (default)
 *    AUD_DAC_DATA_FORMAT_LEFTJ - 16-/20-/24-/32-bit left-justified format
 *    AUD_DAC_DATA_FORMAT_24BIT_RIGHTJ - 24-bit right-justified format
 *    AUD_DAC_DATA_FORMAT_16BIT_RIGHTJ - 16-bit right-justified format
 *    AUD_DAC_DATA_FORMAT_24BIT_I2S_DSP - 24-bit I2S mode DSP format
 *    AUD_DAC_DATA_FORMAT_24BIT_LEFTJ_DSP - 24-bit left-justified mode DSP format
 *    AUD_DAC_DATA_FORMAT_24BIT_I2S_TDM - 24-bit I2S mode TDM format
 *    AUD_DAC_DATA_FORMAT_24BIT_LEFTJ_TDM - 24-bit left-justified mode TDM format
 *    AUD_DAC_DATA_FORMAT_24BIT_HS_I2S_TDM - 24-bit high-speed I2S mode TDM format
 *    AUD_DAC_DATA_FORMAT_24BIT_HS_LEFTJ_TDM - 24-bit high-speed left-justified
 *                                         mode TDM format
 *
 * \return    0 if success.
 *
 **/
AUD_DAC_RET aud_pcm169xDataConfig(Uint8 addr, Uint8 dataFmt);

/**
 * \brief     Sets attenuation mode
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     mode     [IN] Attenuation mode
 *                          0 - Fine step: 0.5-dB step for 0 dB to �63 dB range
 *                          1 - Wide range: 1-dB step for 0 dB to �100 dB range
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xSetAttnMode(Uint8 addr, Uint8 mode);

/**
 * \brief     Reads attenuation mode value
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \return
 *          0 - Fine step: 0.5-dB step for 0 dB to �63 dB range
 *          1 - Wide range: 1-dB step for 0 dB to �100 dB range
 *          AUD_DAC_RET_FAIL - Error in reading attenuation mode value
 *
 **/
Int8 aud_pcm169xGetAttnMode(Uint8 addr);

/**
 * \brief     Configures digital de-emphasis functionality
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \param     rate   [IN] De-emphasis selection
 *                      AUD_DAC_DEEMP_DISABLE  -  Digital de-emphasis disabled
 *                      AUD_DAC_DEEMP_48KHZ    -  Digital de-emphasis 48KHz enabled
 *	                    AUD_DAC_DEEMP_44KHZ    -  Digital de-emphasis 44KHz enabled
 *	                    AUD_DAC_DEEMP_32KHZ    -  Digital de-emphasis 32KHz enabled
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xDeempCtrl(Uint8 addr, Uint8 rate);

/**
 * \brief     Reads DAC register values and displays.
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \return    0 if success.
 *
 **/
AUD_DAC_RET aud_pcm169xRegDump(Uint8 addr);

/**
 * \brief     Selects the sampling mode for PCM169x Codec.
 *
 * \param     addr   [IN] DAC HW instance I2C slave address.
 *
 * \param     mode :
 *            0  - Auto
 *            1  - Single Rate
 *            2  - Dual Rate
 *            3  - Quad Rate
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xSetSamplingMode(Uint8 addr, Uint8 mode);

/**
 * \brief     Enables DAC operation by configuring opmode.
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     modeCtrl [IN] Mode control for DAC channel pair
 *                          0x1 - DAC1/2
 *                          0x2 - DAC3/4
 *                          0x4 - DAC5/6
 *                          0x8 - DAC7/8
 *                          0xF - All channels
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xEnableDacOpeartion(Uint8 addr, Uint8 modeCtrl);

/**
 * \brief     Disables DAC operation by configuring opmode.
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     modeCtrl [IN] Mode control for DAC channel pair
 *                          0x1 - DAC1/2
 *                          0x2 - DAC3/4
 *                          0x4 - DAC5/6
 *                          0x8 - DAC7/8
 *                          0xF - All channels
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xDisableDacOpeartion(Uint8 addr, Uint8 modeCtrl);

/**
 * \brief     Configures filter roll-off
 *
 * \param     addr     [IN] DAC HW instance I2C slave address.
 *
 * \param     chanId   [IN] DAC channel pair ID
 *                          0x1 - DAC1/2
 *                          0x2 - DAC3/4
 *                          0x4 - DAC5/6
 *                          0x8 - DAC7/8
 *                          0xF - All channels
 *
 * \param     rolloff  [IN] Filter roll-off value
 *                          0 - Sharp roll-off
 *                          1 - Slow roll-off
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xSetFilterRolloff(Uint8 addr, Uint8 chanId, Uint8 rolloff);

/**
 * \brief     Enables an AMUTE control event
 *
 * \param     addr    [IN] DAC HW instance I2C slave address.
 *
 * \param     amuteId [IN] Amute control event Id.
 *                         1 - SCKI lost
 *                         2 - Asynchronous detect
 *                         4 - ZERO1 and ZERO2 detect
 *                         8 - DAC disable command
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xEnableAmute (Uint8 addr, Uint8 amuteId);

/**
 * \brief     Disables an AMUTE control event
 *
 * \param     addr    [IN] DAC HW instance I2C slave address.
 *
 * \param     amuteId [IN] Amute control event Id.
 *                         1 - SCKI lost
 *                         2 - Asynchronous detect
 *                         4 - ZERO1 and ZERO2 detect
 *                         8 - DAC disable command
 *
 * \return    0 for success.
 *
 **/
AUD_DAC_RET aud_pcm169xDisableAmute (Uint8 addr, Uint8 amuteId);

#ifdef __cplusplus
}
#endif

#endif  /* _EVMC66X_AUDIO_DC_AUD_DAC_H_ */

/***************************** End Of File ***********************************/
