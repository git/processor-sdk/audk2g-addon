/*
 * Copyright (c) 2015 - 2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      aud_audio.h
 *
 * \brief     Platform audio header file.
 *
 * This file contains structures, typedefs and function prototypes
 * for platform audio module.
 *
 */

#ifndef _AUD_AUDIO_H_
#define _AUD_AUDIO_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef Aud_STATUS
#define Aud_STATUS        int32_t /** Platform API return type */
#endif

/** @defgroup  Platform Audio Macros */
/*@{*/

/** I2C slave address of DAC0 */
#define AUD_AUDIO_DAC0_ADDR    (0x4C)
/** I2C slave address of DAC1 */
#define AUD_AUDIO_DAC1_ADDR    (0x4D)

/** I2C slave address of ADC0 */
#define AUD_AUDIO_ADC0_ADDR    (0x4A)
/** I2C slave address of ADC1 */
#define AUD_AUDIO_ADC1_ADDR    (0x4B)

/** DAC HW instance count */
#define AUD_AUDIO_DAC_COUNT    (2)
/** ADC HW instance count */
#define AUD_AUDIO_AUD_ADC_COUNT    (2)

/** GPIO number for DIR RST pin - GPIO port 1 */
#define AUD_AUDIO_DIR_RST_GPIO       (9)
/** GPIO number for DIR AUDIO pin - GPIO port 0 */
#define AUD_AUDIO_DIR_AUDIO_GPIO     (134)
/** GPIO number for DIR EMPH pin - GPIO port 0 */
#define AUD_AUDIO_DIR_EMPH_GPIO      (135)
/** GPIO number for DIR ERROR pin - GPIO port 0 */
#define AUD_AUDIO_DIR_ERR_GPIO       (136)
/** GPIO number for DIR CLKST pin - GPIO port 0 */
#define AUD_AUDIO_DIR_CLKST_GPIO     (133)
/** GPIO number for DIR FSOUT0 pin - GPIO port 0 */
#define AUD_AUDIO_DIR_FSOUT0_GPIO    (124)
/** GPIO number for DIR FSOUT1 pin - GPIO port 0 */
#define AUD_AUDIO_DIR_FSOUT1_GPIO    (125)

/** GPIO number for McASP clock select pin - GPIO port 0 */
#define AUD_AUDIO_CLK_SEL_GPIO       (132)
/** GPIO number for PCM1690_RST pin - GPIO port 1 */
#define AUD_AUDIO_PCM1690_RST_GPIO    (10)
/** GPIO number for McASP clock select# pin - GPIO port 0 */
#define AUD_AUDIO_CLK_SELz_GPIO      (101)

/** PADCONFIG pin number for DIR RST pin - GPIO port 1 */
#define AUD_AUDIO_DIR_RST_PADCFG       (184)
/** PADCONFIG pin number for DIR AUDIO pin - GPIO port 0 */
#define AUD_AUDIO_DIR_AUDIO_PADCFG     (165)
/** PADCONFIG pin number for DIR EMPH pin - GPIO port 0 */
#define AUD_AUDIO_DIR_EMPH_PADCFG      (166)
/** PADCONFIG pin number for DIR ERROR pin - GPIO port 0 */
#define AUD_AUDIO_DIR_ERR_PADCFG       (167)
/** PADCONFIG pin number for DIR CLKST pin - GPIO port 0 */
#define AUD_AUDIO_DIR_CLKST_PADCFG     (164)
/** PADCONFIG pin number for DIR FSOUT0 pin - GPIO port 0 */
#define AUD_AUDIO_DIR_FSOUT0_PADCFG    (155)
/** PADCONFIG pin number for DIR FSOUT1 pin - GPIO port 0 */
#define AUD_AUDIO_DIR_FSOUT1_PADCFG    (156)

/*@}*/  /* defgroup */

/**
 *  \brief  Enum to choose clock source for DAC and ADC
 */
typedef enum _AudAudioClkSrc
{
	AUD_AUDIO_CLK_SRC_DIR = 0,
	AUD_AUDIO_CLK_SRC_I2S,
	AUD_AUDIO_CLK_SRC_OSC
} AudAudioClkSrc;

/** @defgroup  Platform Audio ADC Enums */
/*@{*/

/**
 *  \brief  Enum to choose HW ADC device
 */
typedef enum _AudAdcDevId
{
    /** Enables HW ADC device instance 0 for the operation */
    AUD_ADC_DEVICE_0 = 0,
    /** Enables HW ADC device instance 1 for the operation */
    AUD_ADC_DEVICE_1,
    /** Enables all the available HW ADC device instances for the operation */
    AUD_ADC_DEVICE_ALL

} AudAdcDevId;

/**
 *  \brief  Enum to indicate ADC channel number
 */
typedef enum _AudAdcChanId
{
    /** ADC channel 1 left */
    AUD_ADC_CH1_LEFT = 0,
    /** ADC channel 1 right */
    AUD_ADC_CH1_RIGHT,
    /** ADC channel 2 left */
    AUD_ADC_CH2_LEFT,
    /** ADC channel 2 right */
    AUD_ADC_CH2_RIGHT,
    /** All the 4 ADC channels */
    AUD_ADC_CH_ALL

} AudAdcChanId;

/**
 *  \brief  ADC left channel input mux selection
 */
typedef enum _AudAdcLeftInputMux
{
    /** ADC left channel input disabled */
	AUD_ADC_INL_NONE = 0x0,
	/** Single ended VINL1 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL1 = 0x1,
	/** Single ended VINL2 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL2 = 0x2,
	/** Single ended VINL2 + VINL1 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL2_VINL1 = 0x3,
	/** Single ended VINL3 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL3 = 0x4,
	/** Single ended VINL3 + VINL1 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL3_VINL1 = 0x5,
	/** Single ended VINL3 + VINL2 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL3_VINL2 = 0x6,
	/** Single ended VINL3 + VINL2 + VINL1 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL3_VINL2_VINL1 = 0x7,
	/** Single ended VINL4 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL4 = 0x8,
	/** Single ended VINL4 + VINL1 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL4_VINL1 = 0x9,
	/** Single ended VINL4 + VINL2 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL4_VINL2 = 0xA,
	/** Single ended VINL4 + VINL2 + VINL1 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL4_VINL2_VINL1 = 0xB,
	/** Single ended VINL4 + VINL3 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL4_VINL3 = 0xC,
	/** Single ended VINL4 + VINL3 + VINL1 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL4_VINL3_VINL1 = 0xD,
	/** Single ended VINL4 + VINL3 + VINL2 is selected as ADC left input */
	AUD_ADC_INL_SE_VINL4_VINL3_VINL2 = 0xE,
	/** Single ended VINL4 + VINL3 + VINL2 + VINL1 is selected
	    as ADC left input */
	AUD_ADC_INL_SE_VINL4_VINL3_VINL2_VINL1 = 0xF,
	/** Differential VIN1P + VIN1M is selected as ADC left input */
	AUD_ADC_INL_DIFF_VIN1P_VIN1M = 0x10,
	/** Differential VIN4P + VIN4M is selected as ADC left input */
	AUD_ADC_INL_DIFF_VIN4P_VIN4M = 0x20,
	/** Differential VIN1P + VIN1M + VIN4P + VIN4M is selected
	    as ADC left input */
	AUD_ADC_INL_DIFF_VIN1P_VIN1M_VIN4P_VIN4M = 0x30

} AudAdcLeftInputMux;

/**
 *  \brief  ADC right channel input mux selection
 */
typedef enum _AudAdcRightInputMux
{
    /** ADC right channel input disabled */
	AUD_ADC_INR_NONE = 0x0,
	/** Single ended VINR1 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR1 = 0x1,
	/** Single ended VINR2 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR2 = 0x2,
	/** Single ended VINR2 + VINR1 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR2_VINR1 = 0x3,
	/** Single ended VINR3 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR3 = 0x4,
	/** Single ended VINR3 + VINR1 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR3_VINR1 = 0x5,
	/** Single ended VINR3 + VINR2 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR3_VINR2 = 0x6,
	/** Single ended VINR3 + VINR2 + VINR1 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR3_VINR2_VINR1 = 0x7,
	/** Single ended VINR4 is selected as ADC right input */
	AUD_ADC_INL_SE_VINR4 = 0x8,
	/** Single ended VINR4 + VINR1 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR4_VINR1 = 0x9,
	/** Single ended VINR4 + VINR2 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR4_VINR2 = 0xA,
	/** Single ended VINR4 + VINR2 + VINR1 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR4_VINR2_VINR1 = 0xB,
	/** Single ended VINR4 + VINR3 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR4_VINR3 = 0xC,
	/** Single ended VINR4 + VINR3 + VINR1 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR4_VINR3_VINR1 = 0xD,
	/** Single ended VINR4 + VINR3 + VINR2 is selected as ADC right input */
	AUD_ADC_INR_SE_VINR4_VINR3_VINR2 = 0xE,
	/** Single ended VINR4 + VINR3 + VINR2 + VINR1 is selected
	    as ADC right input */
	AUD_ADC_INR_SE_VINR4_VINR3_VINR2_VINR1 = 0xF,
	/** Differential VIN2P + VIN2M is selected as ADC right input */
	AUD_ADC_INR_DIFF_VIN2P_VIN2M = 0x10,
	/** Differential VIN4P + VIN4M is selected as ADC right input */
	AUD_ADC_INR_DIFF_VIN3P_VIN3M = 0x20,
	/** Differential VIN2P + VIN2M + VIN3P + VIN3M is selected
	    as ADC right input */
	AUD_ADC_INR_DIFF_VIN2P_VIN2M_VIN3P_VIN3M = 0x30

} AudAdcRightInputMux;

/**
 *  \brief  ADC receive PCM word length selection
 */
typedef enum _AudAdcRxWordLen
{
    /** ADC PCM word length selection for 24 bit */
    AUD_ADC_RX_WLEN_24BIT = 1,
    /** ADC PCM word length selection for 20 bit */
    AUD_ADC_RX_WLEN_20BIT = 2,
    /** ADC PCM word length selection for 16 bit */
    AUD_ADC_RX_WLEN_16BIT = 3

} AudAdcRxWordLen;

/**
 *  \brief  ADC Serial Audio Interface Data Format
 */
typedef enum _AudAdcDataFormat
{
    /** ADC I2S data format */
    AUD_ADC_DATA_FORMAT_I2S = 0,
    /** ADC left justified data format */
    AUD_ADC_DATA_FORMAT_LEFTJ,
    /** ADC right justified data format */
    AUD_ADC_DATA_FORMAT_RIGHTJ,
    /** ADC TDM/DSP data format */
    AUD_ADC_DATA_FORMAT_TDM_DSP

} AudAdcDataFormat;

/**
 *  \brief  ADC power state selection
 */
typedef enum _AudAdcPowerState
{
    /** ADC digital standby state */
    AUD_ADC_POWER_STATE_STANDBY = 0,
    /** ADC device sleep state */
    AUD_ADC_POWER_STATE_SLEEP,
    /** ADC Analog Power Down state */
    AUD_ADC_POWER_STATE_POWERDOWN
} AudAdcPowerState;

/**
 *  \brief  ADC interrupts
 */
typedef enum _AudAdcIntr
{
    /** Energysense Interrupt */
    AUD_ADC_INTR_ENERGY_SENSE = 0,
    /** I2S RX DIN toggle Interrupt */
    AUD_ADC_INTR_DIN_TOGGLE,
    /** DC Level Change Interrupt */
    AUD_ADC_INTR_DC_CHANGE,
    /** Clock Error Interrupt */
    AUD_ADC_INTR_CLK_ERR,
    /** Post-PGA Clipping Interrupt */
    AUD_ADC_INTR_POST_PGA_CLIP,
    /** To controls all the ADC interrupts together */
    AUD_ADC_INTR_ALL

} AudAdcIntr;

/**
 *  \brief  ADC status read options
 */
typedef enum _AudAdcStatus
{
    /** Current Power State of the device */
    AUD_ADC_STATUS_POWER_STATE = 0,
    /** Current Sampling Frequency */
    AUD_ADC_STATUS_SAMPLING_FREQ,
    /** Current receiving BCK ratio */
    AUD_ADC_STATUS_BCK_RATIO,
    /** Current SCK Ratio */
    AUD_ADC_STATUS_SCK_RATIO,
    /** LRCK Halt Status */
    AUD_ADC_STATUS_LRCK_HALT,
    /** BCK Halt Status */
    AUD_ADC_STATUS_BCK_HALT,
    /** SCK Halt Status */
    AUD_ADC_STATUS_SCK_HALT,
    /** LRCK Error Status */
    AUD_ADC_STATUS_LRCK_ERR,
    /** BCK Error Status */
    AUD_ADC_STATUS_BCK_ERR,
    /** SCK Error Status */
    AUD_ADC_STATUS_SCK_ERR,
    /** DVDD Status */
    AUD_ADC_STATUS_DVDD,
    /** AVDD Status */
    AUD_ADC_STATUS_AVDD,
    /** Digital LDO Status */
    AUD_ADC_STATUS_LDO

} AudAdcStatus;

/**
 *  \brief  ADC DSP channel processing configuration
 */
typedef enum _AudAdcDspChanCfg
{
    /** ADC DSP 4 channel mode processing */
    AUD_ADC_DSP_PROC_4CHAN = 0,
    /** ADC DSP 2 channel mode processing */
    AUD_ADC_DSP_PROC_2CHAN

} AudAdcDspChanCfg;

/**
 *  \brief  ADC DSP mixer selection
 */
typedef enum _AudAdcDspMixNum
{
    /** ADC DSP mixer 1 */
    AUD_ADC_DSP_MIX1 = 0,
    /** ADC DSP mixer 2 */
    AUD_ADC_DSP_MIX2,
    /** ADC DSP mixer 3 */
    AUD_ADC_DSP_MIX3,
    /** ADC DSP mixer 4 */
    AUD_ADC_DSP_MIX4,
    /** To control all the DSP mixers together */
    AUD_ADC_DSP_ALL

} AudAdcDspMixNum;

/**
 *  \brief  ADC DSP mixer channel selection
 */
typedef enum _AudAdcDspMixChan
{
    /** ADC DSP mixer channel 1 left */
    AUD_ADC_DSP_MIXCHAN_CH1L = 0,
    /** ADC DSP mixer channel 1 right */
    AUD_ADC_DSP_MIXCHAN_CH1R,
    /** ADC DSP mixer channel 2 left */
    AUD_ADC_DSP_MIXCHAN_CH2L,
    /** ADC DSP mixer channel 2 right */
    AUD_ADC_DSP_MIXCHAN_CH2R,
    /** ADC DSP mixer I2S left */
    AUD_ADC_DSP_MIXCHAN_I2SL,
    /** ADC DSP mixer I2S right */
    AUD_ADC_DSP_MIXCHAN_I2SR,
    /** To control all the mixer channels together */
    AUD_ADC_DSP_MIXCHAN_ALL

} AudAdcDspMixChan;

/*@}*/  /* defgroup */


/** @defgroup  Platform Audio DAC Enums */
/*@{*/

/**
 *  \brief  Enum to choose HW DAC device
 */
typedef enum _AudDacDevId
{
    /** Enables HW DAC device instance 0 for the operation */
    AUD_DAC_DEVICE_0 = 0,
    /** Enables HW DAC device instance 1 for the operation */
    AUD_DAC_DEVICE_1,
    /** Enables all the available HW DAC device instances for the operation */
    AUD_DAC_DEVICE_ALL

} AudDacDevId;

/**
 *  \brief  Enum to indicate DAC internal channels
 */
typedef enum _AudDacChanId
{
    /** DAC internal channel 1 */
    AUD_DAC_CHAN_1 = 0,
    /** DAC internal channel 2 */
    AUD_DAC_CHAN_2,
    /** DAC internal channel 3 */
    AUD_DAC_CHAN_3,
    /** DAC internal channel 4 */
    AUD_DAC_CHAN_4,
    /** DAC internal channel 5 */
    AUD_DAC_CHAN_5,
    /** DAC internal channel 6 */
    AUD_DAC_CHAN_6,
    /** DAC internal channel 7 */
    AUD_DAC_CHAN_7,
    /** DAC internal channel 8 */
    AUD_DAC_CHAN_8,
    /** All DAC internal channels */
    AUD_DAC_CHAN_ALL

} AudDacChanId;

/**
 *  \brief  Enum to indicate DAC internal channel pair
 */
typedef enum _AudDacChanPair
{
    /** DAC internal channel 1 and 2 pair */
    AUD_DAC_CHANP_1_2 = 0,
    /** DAC internal channel 3 and 4 pair */
    AUD_DAC_CHANP_3_4,
    /** DAC internal channel 5 and 6 pair */
    AUD_DAC_CHANP_5_6,
    /** DAC internal channel 7 and 8 pair */
    AUD_DAC_CHANP_7_8,
    /** Indicates all DAC internal channel pairs */
    AUD_DAC_CHANP_ALL

} AudDacChanPair;

/**
 *  \brief  DAC AMUTE control source event selection
 */
typedef enum _AudDacAmuteCtrl
{
    /** Analog mute control by SCKI lost */
    AUD_DAC_AMUTE_CTRL_SCKI_LOST = 0,
    /** Analog mute control by asynchronous detect */
    AUD_DAC_AMUTE_CTRL_ASYNC_DETECT,
    /** Analog mute control by ZERO1 and ZERO2 detect */
    AUD_DAC_AMUTE_CTRL_ZERO_DETECT,
    /** Analog mute control by DAC disable command */
    AUD_DAC_AMUTE_CTRL_DAC_DISABLE_CMD

} AudDacAmuteCtrl;

/**
 *  \brief  DAC sampling mode selection
 */
typedef enum _AudDacSamplingMode
{
    /** Auto sampling mode */
    AUD_DAC_SAMPLING_MODE_AUTO = 0,
    /** Single rate sampling mode */
    AUD_DAC_SAMPLING_MODE_SINGLE_RATE,
    /** Dual rate sampling mode */
    AUD_DAC_SAMPLING_MODE_DUAL_RATE,
    /** Quad rate sampling mode */
    AUD_DAC_SAMPLING_MODE_QUAD_RATE

} AudDacSamplingMode;

/**
 *  \brief  DAC audio interface data format selection
 */
typedef enum _AudDacDataFormat
{
	/** 16-/20-/24-/32-bit I2S format */
    AUD_DAC_DATA_FORMAT_I2S = 0,
    /** 16-/20-/24-/32-bit left-justified format */
    AUD_DAC_DATA_FORMAT_LEFTJ,
    /** 24-bit right-justified format */
    AUD_DAC_DATA_FORMAT_24BIT_RIGHTJ,
    /** 16-bit right-justified format */
    AUD_DAC_DATA_FORMAT_16BIT_RIGHTJ,
    /** 24-bit I2S mode DSP format */
    AUD_DAC_DATA_FORMAT_24BIT_I2S_DSP,
    /** 24-bit left-justified mode DSP format */
    AUD_DAC_DATA_FORMAT_24BIT_LEFTJ_DSP,
    /** 24-bit I2S mode TDM format */
    AUD_DAC_DATA_FORMAT_24BIT_I2S_TDM,
    /** 24-bit left-justified mode TDM format */
    AUD_DAC_DATA_FORMAT_24BIT_LEFTJ_TDM,
    /** 24-bit high-speed I2S mode TDM format */
    AUD_DAC_DATA_FORMAT_24BIT_HS_I2S_TDM,
    /** 24-bit high-speed left-justified mode TDM format */
    AUD_DAC_DATA_FORMAT_24BIT_HS_LEFTJ_TDM

} AudDacDataFormat;

/**
 *  \brief  DAC operation mode selection
 */
typedef enum _AudDacOpMode
{
	/** DAC normal operation mode */
    AUD_DAC_OPMODE_NORMAL = 0,
    /** DAC disabled mode */
    AUD_DAC_OPMODE_DISABLED

} AudDacOpMode;

/**
 *  \brief  DAC digital filter roll-off selection
 */
typedef enum _AudDacFilterRolloff
{
	/** Sharp roll-off for digital filter */
    AUD_DAC_FILTER_SHARP_ROLLOFF = 0,
    /** Slow roll-off for digital filter */
    AUD_DAC_FILTER_SLOW_ROLLOFF

} AudDacFilterRolloff;

/**
 *  \brief  DAC analog signal output phase selection
 */
typedef enum _AudDacOutputPhase
{
	/** Normal phase of DAC analog signal output */
    AUD_DAC_OUTPUT_PHASE_NORMAL = 0,
    /** Inverted phase of DAC analog signal output */
    AUD_DAC_OUTPUT_PHASE_INVERTED

} AudDacOutputPhase;

/**
 *  \brief  DAC digital attenuation mode control selection
 */
typedef enum _AudDacAttnMode
{
	/** Fine step attenuation mode : 0.5-dB step for 0 dB to �63 dB range */
    AUD_DAC_ATTENUATION_FINE_STEP = 0,
    /** Wide range attenuation mode : 1-dB step for 0 dB to �100 dB range */
    AUD_DAC_ATTENUATION_WIDE_RANGE

} AudDacAttnMode;

/**
 *  \brief  DAC Digital de-emphasis function/sampling rate control selection
 */
typedef enum _AudDacDeempCtrl
{
	/** Digital de-emphasis disabled */
    AUD_DAC_DEEMP_DISABLE = 0,
	/** Digital de-emphasis 48KHz enabled */
    AUD_DAC_DEEMP_48KHZ,
    /** Digital de-emphasis 44KHz enabled */
    AUD_DAC_DEEMP_44KHZ,
    /** Digital de-emphasis 32KHz enabled */
    AUD_DAC_DEEMP_32KHZ

} AudDacDeempCtrl;


/*@}*/  /* defgroup */


/** @defgroup  Platform Audio Common Functions */
/*@{*/

Aud_STATUS aud_delay(uint32_t usecs);

/**
 *  \brief    Initializes Audio module
 *
 *  This function configures the system level setup required for
 *  operation of the modules that are available on audio daughter card.
 *  This function shall be called before calling any other platform
 *  audio module functions.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioInit(void);

/**
 *  \brief    Resets audio DAC
 *
 *  This function toggles the GPIO signal connected to RST pin
 *  of DAC module to generate DAC reset.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioResetDac(void);

/**
 *  \brief    Configures audio clock source
 *
 *  McASP can receive clock from DIR module on daughter card or external
 *  I2S device to operate DAC and ADC modules. This function configures
 *  which clock source to use (DIR or I2S) for DAC and ADC operation
 *
 *  \param clkSrc    [IN]  Clock source selection
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioSelectClkSrc(AudAudioClkSrc clkSrc);

/*@}*/  /* defgroup */


/** @defgroup  Platform Audio ADC Functions */
/*@{*/

/**
 *  \brief    Initializes ADC module
 *
 *  This function configures the system level setup required for ADC
 *  operation and initializes the ADC module with default values.
 *  This function should be called before calling any other ADC functions.
 *
 *  After executing this function, ADC module will be ready for audio
 *  processing with default configuration. Default ADC configurations
 *  can be changed using the other ADC APIs if required.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'AUD_ADC_DEVICE_ALL' to initialize
 *                        all the ADC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcInit(AudAdcDevId devId);

/**
 *  \brief    Resets ADC module
 *
 *  This function resets all the ADC module registers to their
 *  HW default values.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'AUD_ADC_DEVICE_ALL' to reset
 *                        all the ADC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcReset(AudAdcDevId devId);

/**
 *  \brief    Configures ADC gain value
 *
 *  Range of the gain is exposed as percentage by this API. Gain
 *  is indicated as percentage of maximum gain ranging from 0 to 100.
 *  0 indicates minimum gain and 100 indicates maximum gain.
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          Use AUD_ADC_CH_ALL to set gain for all the
 *                          ADC channels
 *
 *  \param gain       [IN]  Gain value; 0 to 100
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcSetGain(AudAdcDevId  devId,
                                        AudAdcChanId chanId,
                                        uint8_t   gain);

/**
 *  \brief    Configures ADC analog input selection for left channel
 *
 *  Default input selection of ADC left channels can be modified using
 *  this function.
 *
 *  Default input selection for ADC left channels is listed below
 *    CH1 LEFT  - VINL1
 *    CH2 LEFT  - VINL2
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          AUD_ADC_CH1_LEFT - Input selection for channel 1 left
 *                          AUD_ADC_CH2_LEFT - Input selection for channel 2 left
 *
 *  \param inputMux   [IN]  Input mux configuration
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcSetLeftInputMux(AudAdcDevId        devId,
                                                AudAdcChanId       chanId,
                                                AudAdcLeftInputMux inputMux);

/**
 *  \brief    Configures ADC analog input selection for right channel
 *
 *  Default input selection of ADC right channels can be modified using
 *  this function
 *
 *  Default input selection for ADC right channels is shown below
 *    CH1 RIGHT - VINR1
 *    CH2_RIGHT - VINR2
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          AUD_ADC_CH1_RIGHT - Input selection for channel 1 right
 *                          AUD_ADC_CH2_RIGHT - Input selection for channel 2 right
 *
 *  \param inputMux   [IN]  Input mux configuration
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcSetRightInputMux(AudAdcDevId         devId,
                                                 AudAdcChanId        chanId,
                                                 AudAdcRightInputMux inputMux);

/**
 *  \brief    ADC audio interface data configuration
 *
 *  This function configures serial audio interface data format and
 *  receive PCM word length for ADC
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param wLen       [IN]  ADC data word length
 *
 *  \param format     [IN]  Audio data format
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcDataConfig(AudAdcDevId      devId,
                                           AudAdcRxWordLen  wLen,
                                           AudAdcDataFormat format);

/**
 *  \brief    Enables/Disables ADC channel mute
 *
 *  This function configures mute functionality of each ADC channel
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          Use AUD_ADC_CH_ALL to apply mute configuration for
 *                          all the ADC channels
 *
 *  \param muteEnable [IN]  Flag to configure mute
 *                          1 - Mute ADC channel
 *                          0 - Unmute ADC channel
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcMuteCtrl(AudAdcDevId  devId,
                                         AudAdcChanId chanId,
                                         uint8_t   muteEnable);

/**
 *  \brief    Configures ADC MIC bias
 *
 *  This function enables/disables MIC bias for analog MIC input
 *
 *  \param devId         [IN]  Device ID of ADC HW instance
 *                             Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                             for all the ADC devices available.
 *
 *  \param micBiasEnable [IN]  Mic Bias enable flag
 *                             1 - Enable MIC Bias
 *                             0 - Disable MIC Bias
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcMicBiasCtrl(AudAdcDevId devId,
                                            uint8_t  micBiasEnable);

/**
 *  \brief    Configures ADC power state
 *
 *  This function enables/disables different power modes supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param powState    [IN]  ADC power state to configure
 *
 *  \param stateEnable [IN]  Power state enable flag
 *                           1 - Enables the power state
 *                           0 - Disables the power state
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcConfigPowState(AudAdcDevId      devId,
                                               AudAdcPowerState powState,
                                               uint8_t       stateEnable);

/**
 *  \brief    Configures ADC interrupts
 *
 *  This function enables/disables different interrupts supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param intId       [IN]  Interrupt Id to configure
 *                           Use to 'AUD_ADC_INTR_ALL' to configure all the
 *                           interrupts together
 *
 *  \param intEnable   [IN]  Interrupt enable flag
 *                           1 - Enables the interrupt
 *                           0 - Disables the interrupt
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcConfigIntr(AudAdcDevId devId,
                                           AudAdcIntr  intId,
                                           uint8_t  intEnable);

/**
 *  \brief    Reads ADC interrupt status
 *
 *  This function reads the status of different interrupts supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *
 *  \param intId       [IN]  Interrupt Id to read the status
 *                           Use to 'AUD_ADC_INTR_ALL' to read status of all the
 *                           interrupts together
 *
 *  \return    Interrupt status
 *             1 - Interrupt occurred
 *             0 - No interrupt occurred
 */
uint8_t aud_AudioAdcGetIntrStatus(AudAdcDevId devId,
                                      AudAdcIntr  intId);

/**
 *  \brief    Reads ADC status bits
 *
 *  This function reads the value of different status functions supported
 *  by ADC module (excluding interrupts).
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *
 *  \param status      [IN]  Status function of which status to be read
 *
 *  \return    Value of status function
 *
 */
uint8_t aud_AudioAdcGetStatus(AudAdcDevId  devId,
                                  AudAdcStatus status);

/**
 *  \brief    ADC DSP channel configuration control
 *
 *  This function configures the DSP module processing channels
 *  supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param chanCfg     [IN]  DSP channel configuration
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioAdcDspCtrl(AudAdcDevId      devId,
                                        AudAdcDspChanCfg chanCfg);

/**
 *  \brief    Programs ADC DSP coefficients
 *
 *  ADC module supports an internal DSP which performs additional audio
 *  processing operations like mixing, LPF/HPF etc.
 *  DSP coefficients can be programmed by this function.
 *
 *  \param devId        [IN]  Device ID of ADC HW instance
 *                            Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                            for all the ADC devices available.
 *
 *  \param coeffRegAddr [IN]  Address of DSP coefficient register
 *
 *  \param dspCoeff     [IN]  Value of DSP coefficient
 *                            Lower 24 bits are written to DSP coeff register
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioAdcProgDspCoeff(AudAdcDevId      devId,
                                             uint8_t       coeffRegAddr,
                                             uint32_t      dspCoeff);

/**
 *  \brief    Displays ADC register programmed values
 *
 *  This function is provided for debug purpose to read the value
 *  of ADC registers. Values read from the ADC registers will be displayed
 *  in CCS output window or serial terminal based on the system level
 *  configuration for debug messages.
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to read the register values
 *                           for all the ADC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcGetRegDump(AudAdcDevId devId);


/*@}*/  /* defgroup */


/** @defgroup  Platform Audio DAC Functions */
/*@{*/

/**
 *  \brief    Initializes DAC module
 *
 *  This function configures the system level setup required for DAC
 *  operation and initializes the DAC module with default values.
 *  This function should be called before calling any other DAC functions.
 *
 *  After executing this function, DAC module should be ready for audio
 *  processing with default configuration. Default DAC configurations
 *  can be changed using the other DAC APIs if required.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to initialize
 *                            all the DAC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacInit(AudDacDevId devId);

/**
 *  \brief    Resets DAC module
 *
 *  Resetting the DAC module restarts the re-synchronization between
 *  system clock and sampling clock, and DAC operation.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to reset
 *                            all the DAC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacReset(AudDacDevId devId);

/**
 *  \brief    Configures DAC Analog mute control
 *
 *  DAC module supports AMUTE functionality which causes the DAC output
 *  to cut-off from the digital input upon the occurrence of any events
 *  which are configured by AMUTE control.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to apply the configuration
 *                            for all the DAC devices available.
 *
 *  \param muteCtrl     [IN]  Analog mute control event
 *
 *  \param muteEnable   [IN]  Flag to configure AMUTE for given control event
 *                            1 - Enable AMUTE control
 *                            0 - Disable AMUTE control
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacAmuteCtrl(AudDacDevId     devId,
                                          AudDacAmuteCtrl muteCtrl,
                                          uint8_t      muteEnable);

/**
 *  \brief    Configures DAC Audio sampling mode
 *
 *  By default DAC module sampling mode is configured for auto mode.
 *  In Auto mode, the sampling mode is automatically set according to multiples
 *  between the system clock and sampling clock. Single rate for 512 fS, 768 fS,
 *  and 1152 fS, dual rate for 256 fS or 384 fS, and quad rate for 128 fS
 *  and 192 fS. Setting the sampling mode is required only if auto mode
 *  configurations are not suitable for the application.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to apply the configuration
 *                            for all the DAC devices available.
 *
 *  \param samplingMode [IN]  DAC audio sampling mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetSamplingMode(AudDacDevId        devId,
                                                AudDacSamplingMode samplingMode);

/**
 *  \brief    Configures DAC Audio interface data format
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param dataFormat [IN]  DAC audio data format
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetDataFormat(AudDacDevId      devId,
                                              AudDacDataFormat dataFormat);

/**
 *  \brief    Configures DAC operation mode
 *
 *  This function configures a particular DAC channel pair to be operating
 *  normal or disabled.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanPair   [IN]  Internal DAC channel pair
 *                          Use AUD_DAC_CHANP_1_2 to AUD_DAC_CHANP_7_8 for
 *                          individual DAC channel pair configuration
 *                          Use AUD_DAC_CHANP_ALL to set operation mode
 *                          for all DAC channels
 *
 *  \param opMode     [IN]  DAC operation mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetOpMode(AudDacDevId    devId,
                                          AudDacChanPair chanPair,
                                          AudDacOpMode   opMode);

/**
 *  \brief    Configures DAC filter roll-off
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanPair   [IN]  Internal DAC channel pair
 *                          Use macros AUD_DAC_CHANP_1_2 to AUD_DAC_CHANP_7_8 for
 *                          individual DAC channel pair configuration
 *                          Use macro AUD_DAC_CHANP_ALL to set filter roll-off
 *                          for all DAC channels
 *
 *  \param rolloff    [IN]  Roll-off configuration
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetFilterRolloff(AudDacDevId         devId,
                                                 AudDacChanPair      chanPair,
                                                 AudDacFilterRolloff rolloff);

/**
 *  \brief    Configures phase of the DAC analog signal outputs
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanId     [IN]  Internal DAC channel Id
 *                          Use AUD_DAC_CHAN_1 to AUD_DAC_CHAN_8 for individual
 *                          DAC channel configuration
 *                          Use AUD_DAC_CHAN_ALL to set output phase for all
 *                          DAC channels
 *
 *  \param outPhase   [IN]  Mute enable flag
 *                          0 for unmute and 1 for mute
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetOutputPhase(AudDacDevId       devId,
                                               AudDacChanId      chanId,
                                               AudDacOutputPhase outPhase);

/**
 *  \brief    Soft mute function control
 *
 *  The Soft mute function allows mute/unmute of DAC output in gradual steps.
 *  This configuration reduces pop and zipper noise during muting of the
 *  DAC output.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanId     [IN]  Internal DAC channel Id
 *                          Use macros AUD_DAC_CHAN_1 to AUD_DAC_CHAN_8 for individual
 *                          DAC channel configuration
 *                          Use macro AUD_DAC_CHAN_ALL to mute/unmute all DAC
 *                          channels
 *
 *  \param muteEnable [IN]  Mute enable flag
 *                          0 for unmute and 1 for mute
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSoftMuteCtrl(AudDacDevId   devId,
                                             AudDacChanId  chanId,
                                             uint8_t    muteEnable);

/**
 *  \brief    Sets attenuation mode
 *
 *  DAC module supports two types of volume/attenuation dB range
 *  which can be changed by setting attenuation mode. Volume range and fine
 *  tuning will change based on the attenuation mode.
 *
 *  \param devId    [IN]  Device ID of DAC HW instance
 *                        Use 'DAC_DEVICE_ALL' to apply the configuration
 *                        for all the DAC devices available.
 *
 *  \param attnMode [IN]  Attenuation mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetAttnMode(AudDacDevId    devId,
                                            AudDacAttnMode attnMode);

/**
 *  \brief    Function to control digital de-emphasis functionality
 *
 *  Disables/Enables the various sampling frequencies of the digital
 *  de-emphasis function.
 *
 *  \param devId     [IN]  Device ID of DAC HW instance
 *                         Use 'DAC_DEVICE_ALL' to apply the configuration
 *                         for all the DAC devices available.
 *
 *  \param deempCtrl [IN]  De-emphasis control options
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacDeempCtrl(AudDacDevId     devId,
                                          AudDacDeempCtrl deempCtrl);

/**
 *  \brief    Configures DAC volume/attenuation
 *
 *  Range of the volume is exposed as percentage by this API. Volume
 *  is indicated as percentage of maximum value ranging from 0 to 100.
 *  0 to mute the volume and 100 to set maximum volume
 *
 *  DAC module supports two types of volume/attenuation dB range
 *  which can be changed using aud_AudioDacSetAttnMode().
 *  Volume range and fine tuning will change based on the attenuation mode.
 *
 *  \param devId  [IN]  Device ID of DAC HW instance
 *                      Use 'DAC_DEVICE_ALL' to apply the configuration
 *                      for all the DAC devices available.
 *
 *  \param chanId [IN]  Internal DAC channel Id
 *                      Use AUD_DAC_CHAN_1 to AUD_DAC_CHAN_8 for individual
 *                      DAC channel configuration
 *                      Use AUD_DAC_CHAN_ALL to mute/unmute all DAC
 *                      channels
 *
 *  \param volume [IN]  Volume in percentage; 0 to 100
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetVolume(AudDacDevId devId,
                                          AudDacChanId chanId,
                                          uint8_t  volume);

/**
 *  \brief    Configures DAC power-save mode
 *
 *  \param devId     [IN]  Device ID of DAC HW instance
 *                         Use 'DAC_DEVICE_ALL' to apply the configuration
 *                         for all the DAC devices available.
 *
 *  \param PowerMode [IN]  Power-save mode control
 *                         0 - Enable power-save mode
 *                         1 - Disable power-save mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetPowerMode(AudDacDevId devId,
                                             uint8_t  PowerMode);

/**
 *  \brief    Displays DAC register programmed values
 *
 *  This function is provided for debug purpose to read the value
 *  of DAC registers. Values read from the DAC registers will be displayed
 *  in CCS output window or serial terminal based on the system level
 *  configuration for debug messages.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacGetRegDump(AudDacDevId devId);

/*@}*/  /* defgroup */


/** @defgroup  Platform Audio DIR Functions */
/*@{*/

/**
 *  \brief    Initializes DIR module
 *
 *  Configures GPIOs and other settings required for DIR module operation.
 *  This function should be called before calling any other DIR functions.
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioDirInit(void);

/**
 *  \brief    Resets DIR module
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioDirReset(void);

/**
 *  \brief    Reads AUDIO output status value of the DIR
 *
 *  DIR AUDIO output pin gives the audio sample word information
 *  of the channel-status data bit 1
 *
 *  \return AUDIO pin output with below possible values
 *  \n      0 - Audio sample word represents linear PCM samples
 *  \n      1 - Audio sample word is used for other purposes
 */
int8_t aud_AudioDirGetAudioStatus(void);

/**
 *  \brief    Reads EMPH output status value of the DIR
 *
 *  DIR EMPH output pin gives the emphasis information of the
 *  channel-status data bit 3.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Two audio channels without pre-emphasis
 *  \n      1 - Two audio channels with 50 ms / 15 ms pre-emphasis
 */
int8_t aud_AudioDirGetEmphStatus(void);

/**
 *  \brief    Reads ERROR pin status value of the DIR
 *
 *  DIR ERROR output pin gives the error state of data and parity errors.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Lock state of PLL and nondetection of parity error
 *  \n      1 - Unlock state of PLL or detection of parity error
 */
int8_t aud_AudioDirGetErrStatus(void);

/**
 *  \brief    Reads CLKST pin status value of the DIR
 *
 *  DIR CLKST pin outputs the PLL status change between LOCK and UNLOCK.
 *  The CLKST output pulse depends only on the status change of the PLL.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Lock state of PLL and nondetection of parity error
 *  \n      1 - Unlock state of PLL or detection of parity error
 */
int8_t aud_AudioDirGetClkStatus(void);

/**
 *  \brief    Reads FSOUT[1:0] output status value of the DIR
 *
 *  The DIR module calculates the actual sampling frequency of the
 *  biphase input signal and outputs its result through FSOUT[1:0] pins.
 *
 *  \return FSOUT pin output with below possible values
 *  \n      0 - Calculated Sampling Frequency Output is 43 kHz�45.2 kHz
 *  \n      1 - Calculated Sampling Frequency Output is 46.8 kHz�49.2 kHz
 *  \n      2 - Out of range or PLL unlocked
 *  \n      3 - Calculated Sampling Frequency Output is 31.2 kHz�32.8 kHz
 */
int8_t aud_AudioDirGetFsOut(void);

/*@}*/  /* defgroup */

#ifdef __cplusplus
}
#endif

#endif /* _AUD_AUDIO_H_ */

/* Nothing past this point */
