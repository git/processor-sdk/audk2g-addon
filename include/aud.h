/*
 * Copyright (c) 2011-2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 *  @file   aud.h
 *
 *  @brief  Private definitions for the Platform Library.
 *
 *  ============================================================================
 */

#ifndef _AUD_INTERNAL_H_
#define _AUD_INTERNAL_H_

/**
 * Error codes used by Platform functions. Negative values are errors,
 * while positive values indicate success.
 */

#define Aud_STATUS        int32_t /** Platform API return type */

#define Aud_EINVALID     -3   /**< Error code for invalid parameters */
#define Aud_EUNSUPPORTED -2   /**< Error code for unsupported feature */
#define Aud_EFAIL        -1   /**< General failure code */
#define Aud_EOK           0   /**< General success code */

/********************************************************************************************
 * 					Includes for the Library Routines										*
 *******************************************************************************************/

#include "types.h"
#include <ti/csl/csl_types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ti/csl/cslr_gpmc.h>
#include <ti/csl/cslr_elm.h>
#include <ti/csl/csl_gpioAux.h>
#include <ti/csl/csl_bootcfgAux.h>
#include <ti/csl/csl_chipAux.h>
#include <ti/csl/cslr_mmchs.h>
#include <ti/csl/cslr_qspi.h>
#include <ti/csl/cslr_ecap.h>
#include <ti/csl/cslr_pwmss.h>
#include <ti/csl/cslr_mcasp.h>

#include "evmc66x_i2c.h"
#include "evmc66x_gpio.h"
#include "evmc66x_audio_dc_dac.h"
#include "evmc66x_audio_dc_adc.h"
#include "evmc66x_pinmux.h"
#include "aud_audio.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Indicates the type of device
 *
 */
typedef enum {
    AUD_DEVICE_NAND,
    /**<NAND Flash*/
    AUD_DEVICE_NOR,
    /**<NOR Flash*/
    AUD_DEVICE_EEPROM,
    /**<EEPROM */
#ifdef SOC_K2G
    AUD_DEVICE_SD,
    /**<SD Card*/
    AUD_DEVICE_EMMC,
    /**<eMMC Card*/
    AUD_DEVICE_QSPI_FLASH,
    /**<QSPI flash */
#endif
    AUD_DEVICE_MAX
    /**<End of devices*/
} AUD_DEVICE_TYPE;

/**
 *  @brief 	This type defines the opaque handle returned to a device that is opened.
 *  		The handle must be used in all subsequent operations.
 *
 */
typedef uint32_t AUD_DEVHANDLE;

/**
 *  @brief This structure contains information about the flash device on the platform
 *
 *			The bblist points to an array of bytes where each position represents a
 *			block on the device. If the block is good it is marked as 0xFF. If the block
 *			is bad, it is marked as 0x00. For devices that do not support a bad block list this
 *			value will be NULL. The number of blocks in the bblist is determined by the block_count field.
 */
typedef struct {
    int32_t manufacturer_id;		/**<manufacturer ID*/
    int32_t device_id;				/**<Manufacturers device ID*/
    AUD_DEVICE_TYPE  type;		/**<Type of device */
    int32_t width;					/**<Width in bits*/
    int32_t block_count;			/**<Total blocks. First block starts at 0. */
    int32_t page_count;				/**<Page count per block*/
    int32_t page_size;				/**<Number of bytes in a page including spare area*/
    int32_t spare_size;				/**<Spare area size in bytes*/
    AUD_DEVHANDLE handle;		/**<Handle to the block device as returned by Open. Handle is Opaque, do not interpret or modify */
    int32_t	bboffset;				/**<Offset into spare area to check for a bad block */
	uint32_t column;				/**<Column for a NAND device */
	uint32_t flags;					/**<Flags is a copy of the flags that were used to open the device */
	void	*internal;				/**<Do not use. Used internally by the platform library */
    uint8_t *bblist;				/** <Bad Block list or NULL if device does not support one  */
} AUD_DEVICE_info;

/********************************************************************************************
 * 					BUILD OPTIONS FOR THE LIBRARY											*
 *******************************************************************************************/

 /**
 *  @brief aud_errno variable may be set to a non-zero value when
 *         a platform library call returns an error.
 *
 *      The errno value is not preserved. The calling application must
 *       save off the value if the platform function call fails.
 */
extern uint32_t aud_errno;
extern uint32_t aud_errno2;

/** Platform errno values */

#define AUD_ERRNO_RESET            0				/**< No error */

#define AUD_ERRNO_GENERIC          0x00000001
#define AUD_ERRNO_INVALID_ARGUMENT 0x00000002		/**< NULL pointer, argument out of range, etc									*/

#define AUD_ERRNO_PLL_SETUP        0x00000003		/**< Error initializing	*/
#define AUD_ERRNO_EEPROM           0x00000004		/**< Error initializing	*/
#define AUD_ERRNO_UART             0x00000005		/**< Error initializing	*/
#define AUD_ERRNO_LED              0x00000006		/**<  Error initializing	*/
#define AUD_ERRNO_I2C              0x00000007		/**<  Error initializing	*/
#define AUD_ERRNO_MEMTEST          0x00000008		/**<  Error initializing	*/
#define AUD_ERRNO_PHY              0x00000009		/**<  Error initializing	*/
#define AUD_ERRNO_NAND             0x0000000a		/**<  Error initializing	*/
#define AUD_ERRNO_NOR              0x0000000b		/**<  Generic error in NOR              */
#define	AUD_ERRNO_UNSUPPORTED		0x0000000c		/**<  Functionality not supported */

#define AUD_ERRNO_ECC_FAIL			0x00000010		/**<  The ECC calculation for the page read doesn't match. The application can try re-reading. */
#define AUD_ERRNO_BADFLASHDEV		0x00000011		/**<  The flash routines did no not recognize the flash manufacturer			*/
#define	AUD_ERRNO_FLASHADDR		0x00000012		/**<  The block or page number specified does not exist for the Flash			*/
#define	AUD_ERRNO_NANDBBT			0x00000013		/**<  Could not update the NAND Bad Block Table								*/
#define AUD_ERRNO_NORADDR          0x00000014		/**<  Address for NOR does not exist											*/
#define AUD_ERRNO_NOFREEBLOCKS     0x00000015		/**<  There were not enough consecutive free blocks to write the data (based on your starting block number)*/

#define AUD_ERRNO_DEV_TIMEOUT		0x00000020		/**<  There was an idle timeout waiting on a device action 					*/
#define AUD_ERRNO_DEV_NAK			0x00000021		/**<  The device NAK'd the command												*/
#define AUD_ERRNO_DEV_BUSY			0x00000022		/**<  The device reported a busy state and could not complete the operation	*/
#define AUD_ERRNO_DEV_FAIL			0x00000023		/**<  Device returned a failed status											*/
#define AUD_ERRNO_PSCMOD_ENABLE	0x00000024		/**<  Unable to enable the PSC Module											*/

#define AUD_ERRNO_OOM				0x00000030		/**<  Out of memory.. tried to allocate RAM but could not.						*/

#define AUD_ERRNO_READTO			0x00000040		/**<  UART read timeout 														*/

#define AUD_ERRNO_AUDIO			0x00000050		/**<  Generic in Audio module 									                */
#define AUD_ERRNO_AUDIO_INIT	    0x00000051		/**<  Error in Audio module initialization									    */
#define AUD_ERRNO_AUDIO_CFG		0x00000052		/**<  Error in Audio module configuration									    */
#define AUD_ERRNO_AUDIO_STATUS	    0x00000053		/**<  Error in readig Audio module status */
 
/***
 * @brief  The following flags are used for controlling the build of Platform Library
 */

/*  brief Platform Library version number */
#define AUD_LIB_VERSION "3.02.00.00"

/* Turn on and off debug statements (may not be safe in certain contexts) */
#define	AUD_DEBUG	0

/* These flags compile in and out functionality offered by the library allowing you to control
 * the size of the code that is included.
 */
#ifdef _AUD_LITE_
	/*
	 * Build a version of Platform Library suitable for use in an EEPROM or other small application.
	 * (likely needs tailoring to the specific application if size matters)
	 */
	#define	AUD_VERSTRING_IN		  1
	#define	AUD_INIT_IN			  1
	#define	AUD_GETINFO_IN			  1
	#define AUD_GETCOREID_IN		  1
	#define AUD_GETSWITCHSTATE_IN	  0
	#define AUD_GETMACADDR_IN		  1
	#define AUD_GETPHYADDR_IN		  0
	#define AUD_PHYLINKSTATUS_IN	  0
	#define AUD_DELAY_IN			  1
	#define AUD_LED_IN				  1
	#define AUD_WRITE_IN			  0
	#define AUD_READ_IN              0
	#define	AUD_EXTMEMTEST_IN		  1
	#define	AUD_I2C_EEPROM_IN	      1
	#define	AUD_I2C_EEPROM_WRITE_IN  1
	#define	AUD_UART_IN			  1
	#define	AUD_NOR_IN				  1
	#define	AUD_NOR_READ_IN		  1
	#define	AUD_NOR_WRITE_IN		  0
	#define AUD_NAND_IN			  1
	#define AUD_NAND_READ_IN		  1
	#define AUD_NAND_WRITE_IN		  0
    #define AUD_NAND_ECC_IN          0
	#define AUD_SEMLOCK_IN			  0
	#define AUD_CACHE_IN			  0
	#define AUD_PLAT_GPIO_IN 			  1
	#define AUD_I2C_IN 			  1
	#define AUD_I2C_IO_EXP_IN 		  1
	#define AUD_AUDIO                1
	#define AUD_AUDIO_ADC            1
	#define AUD_AUDIO_DAC            1
	#define AUD_AUDIO_DIR            1
    #define AUD_MMCHS_IN             1
    #define AUD_DSS_IN               0
	#define AUD_DCAN_IN              0
    #define AUD_QSPI_FLASH_IN        1
    #define AUD_QSPI_FLASH_READ_IN   1
    #define AUD_QSPI_FLASH_WRITE_IN  1
    #define AUD_MCASP_IN             0
#else
	/*
	 * Build the FULL version of Platform Library
	 */
	#define	AUD_VERSTRING_IN		  1
	#define	AUD_INIT_IN			  1
	#define	AUD_GETINFO_IN			  1
	#define AUD_GETCOREID_IN		  1
	#define AUD_GETSWITCHSTATE_IN	  1
	#define AUD_GETMACADDR_IN		  1
	#define AUD_GETPHYADDR_IN		  1
	#define AUD_PHYLINKSTATUS_IN	  1
	#define AUD_DELAY_IN			  1
	#define AUD_LED_IN				  1
	#define AUD_WRITE_IN			  1
	#define AUD_READ_IN              1
	#define	AUD_EXTMEMTEST_IN		  1
	#define	AUD_I2C_EEPROM_WRITE_IN  1
	#define	AUD_I2C_EEPROM_IN		  1
	#define	AUD_UART_IN			  1
	#define	AUD_NOR_IN				  1
	#define	AUD_NOR_READ_IN		  1
	#define	AUD_NOR_WRITE_IN		  1
	#define AUD_NAND_IN			  1
	#define AUD_NAND_READ_IN		  1
	#define AUD_NAND_WRITE_IN		  1
    #define AUD_NAND_ECC_IN          0
	#define AUD_SEMLOCK_IN			  1
	#define AUD_CACHE_IN			  1
	#define AUD_PLAT_GPIO_IN 			  1
	#define AUD_I2C_IN 			  1
	#define AUD_I2C_IO_EXP_IN 		  1
	#define AUD_AUDIO                1
	#define AUD_AUDIO_ADC            1
	#define AUD_AUDIO_DAC            1
	#define AUD_AUDIO_DIR            1
    #define AUD_MMCHS_IN             1
    #define AUD_DSS_IN               1
	#define AUD_DCAN_IN              1
    #define AUD_QSPI_FLASH_IN        1
    #define AUD_QSPI_FLASH_READ_IN   1
    #define AUD_QSPI_FLASH_WRITE_IN  1
    #define AUD_MCASP_IN             1
#endif

#if (AUD_DEBUG && !AUD_WRITE_IN)
#error	You must enable AUD_WRITE to turn on DEBUG
#endif

#if (AUD_DEBUG)
#define IFPRINT(x)   (x)
#else
#define IFPRINT(x)
#endif

/* This flag implements a workaround to re-initialize PLL and DDR
   if DDR test after DDR initialization */
#undef AUD_PLL_REINIT
#define PLL_REINIT_DDR3_TEST_START_ADDR (0x80000000)
#define PLL_REINIT_DDR3_TEST_END_ADDR   (PLL_REINIT_DDR3_TEST_START_ADDR + (128 *1024))

/********************************************************************************************
 * 					Platform Specific Declarations											*
 *******************************************************************************************/

#define AUD_INFO_CPU_NAME 		"TCI66AK2G02"
#define AUD_INFO_BOARD_NAME 	"TMDXEVM66AK2G02"

#define MEGM_REV_ID_REGISTER   (0x01812000)
#define MEGM_REV_ID_MAJ_MASK   (0xFFFF0000)
#define MEGM_REV_ID_MAJ_SHIFT  (16)
#define MEGM_REV_ID_MIN_MASK   (0x0000FFFF)
#define MEGM_REV_ID_MIN_SHIFT  (0)

/* Default UART baudrate value */
#define  AUD_UART_BAUDRATE_val (19200)

/* LED Definitions */
#define AUD_BMC_LED_COUNT   (4)
#define AUD_SOC_LED_COUNT   (5)
#define AUD_I2C_LED_COUNT   (0)
#define AUD_TOTAL_LED_COUNT (AUD_BMC_LED_COUNT + \
                                  AUD_I2C_LED_COUNT + \
                                  AUD_SOC_LED_COUNT)

#define AUD_SOC_LED0        (0)
#define AUD_SOC_LED1        (1)
#define AUD_SOC_LED2        (2)
#define AUD_SOC_LED3        (3)
#define AUD_SOC_LED4        (4)

#define AUD_SOC_LED0_GPIO            (AUD_GPIO_PIN_108)
#define AUD_SOC_LED1_GPIO            (AUD_GPIO_PIN_11)
#define AUD_SOC_LED0_PADCONFIG       (139)
#define AUD_SOC_LED1_PADCONFIG       (186)

/* Number of cores on the platform */
#define AUD_CORE_COUNT      (1)

/* Memory Sections */
#define AUD_L1P_BASE_ADDRESS (CSL_C66X_COREPAC_0_L1P_SRAM_REGS)
#define AUD_L1P_SIZE         (CSL_C66X_COREPAC_0_L1P_SRAM_REGS_SIZE)    /* 32K bytes */
#define AUD_L1D_BASE_ADDRESS (CSL_GEM_INTERNAL_DSP0_L1D_SRAM_RAM_REGS)
#define AUD_L1D_SIZE         (CSL_GEM_INTERNAL_DSP0_L1D_SRAM_RAM_REGS_SIZE)    /* 32K bytes */
#define AUD_DDR3_SDRAM_START 0x80000000
#define AUD_DDR3_SDRAM_END   0x9FFFFFFF /* DDR3A 1024 MB*/

/* AT24CM01 EEPROM */
#define AUD_I2C_EEPROM_MANUFACTURER_ID (0x01)
#define AUD_I2C_EEPROM_DEVICE_ID_1     (0x50)
#define AUD_I2C_EEPROM_DEVICE_ID_2     (0x51)

/* UART port number for sending debug messages */
/* Set to UART_PORT_0 for DB9 connector and UART_PORT_2 for console header */
#define AUD_UART_DBG_PORT    (UART_PORT_0)

/********************************************************************************************
 * 					General Declarations													*
 *******************************************************************************************/

/* Macro to calclate the size of default register Array */
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

/* Size of a string we can output with aud_write */
#define	MAX_WRITE_LEN	200

/* Device Tables */
#if (AUD_NAND_IN)
extern AUD_DEVICE_info aud_gDeviceNand;
#endif

#if (AUD_NOR_IN)
extern  AUD_DEVICE_info aud_gDeviceNor;
#endif

#if (AUD_I2C_EEPROM_IN)
extern AUD_DEVICE_info aud_gDeviceEeprom0;
extern AUD_DEVICE_info aud_gDeviceEeprom1;
#endif

#define AUD_READ_DELAY   (30000000)

/**
 *  Handle to access PSC registers.
 */
#define hPscCfg     ((CSL_PscRegs*)CSL_PSC_REGS)

/* Boot Cfg Registers */
#define   DEVSTAT_REG           (*((volatile uint32_t *) 0x02620020))

#define MAX_SN_SIZE             (10)      /* Maximum number of the chars of Serial Number for the EVM */
#define MAX_SN_STORE_SIZE       (128)     /* Maximum size in bytes to store the serial number info */

/* Size of MAC address in EEPROM at slave address 0x51 */
#define MACADDRESS_SIZE			(12)
/* Offset of MAC address in EEPROM at slave address 0x51 */
#define MAC_OFFSET				(0xF400)

/* Registers to enable or disable memory ECC for L1, L2 and MSMC memories */
#define L1PEDSTAT	(0x01846404)
#define L1PEDCMD	(0x01846408)
#define L1PEDADDR	(0x0184640C)
#define L2EDSTAT	(0x01846004)
#define L2EDCMD		(0x01846008)
#define L2EDADDR	(0x0184600C)
#define L2EDCPEC	(0x01846018)
#define L2EDCNEC	(0x0184601C)
#define L2EDCEN		(0x01846030)
#define SMCERRAR	(0x0BC00008)
#define	SMCERRXR	(0x0BC0000C)
#define SMEDCC		(0x0BC00010)
#define SMCEA		(0x0BC00014)
#define SMSECC		(0x0BC00018)

#define DDR_CFG_DELAY    (200)

/* DDR3A definitions */
#define DDR3A_EMIF_CTRL_BASE		(0x21010000)
#define DDR3A_EMIF_DATA_BASE		(0x80000000)
#define DDR3A_DDRPHYC				(0x02329000)


#define DDRPHY_PIR_OFFSET			(0x04)
#define DDRPHY_PGCR0_OFFSET			(0x08)
#define DDRPHY_PGCR1_OFFSET			(0x0C)
#define DDRPHY_PGSR0_OFFSET			(0x10)
#define DDRPHY_PGSR1_OFFSET			(0x14)
#define DDRPHY_PLLCR_OFFSET			(0x18)
#define DDRPHY_PTR0_OFFSET			(0x1C)
#define DDRPHY_PTR1_OFFSET			(0x20)
#define DDRPHY_PTR2_OFFSET			(0x24)
#define DDRPHY_PTR3_OFFSET			(0x28)
#define DDRPHY_PTR4_OFFSET			(0x2C)
#define DDRPHY_DCR_OFFSET			(0x44)

#define DDRPHY_DTPR0_OFFSET			(0x48)
#define DDRPHY_DTPR1_OFFSET			(0x4C)
#define DDRPHY_DTPR2_OFFSET			(0x50)

#define DDRPHY_MR0_OFFSET			(0x54)
#define DDRPHY_MR1_OFFSET			(0x58)
#define DDRPHY_MR2_OFFSET			(0x5C)
#define DDRPHY_DTCR_OFFSET			(0x68)
#define DDRPHY_PGCR2_OFFSET			(0x8C)

#define DDRPHY_ZQ0CR1_OFFSET		(0x184)
#define DDRPHY_ZQ1CR1_OFFSET  		(0x194)
#define DDRPHY_ZQ2CR1_OFFSET		(0x1A4)
#define DDRPHY_ZQ3CR1_OFFSET		(0x1B4)

#define DDRPHY_DATX8_4_OFFSET 		(0x2C0)
#define DDRPHY_DATX8_5_OFFSET 		(0x300)
#define DDRPHY_DATX8_6_OFFSET 		(0x340)
#define DDRPHY_DATX8_7_OFFSET 		(0x380)
#define DDRPHY_DATX8_8_OFFSET 		(0x3C0)

#define DDR3_MIDR_OFFSET		(0x00)
#define DDR3_STATUS_OFFSET		(0x04)
#define DDR3_SDCFG_OFFSET		(0x08)
#define DDR3_SDRFC_OFFSET		(0x10)
#define DDR3_SDTIM1_OFFSET		(0x18)
#define DDR3_SDTIM2_OFFSET		(0x1C)
#define DDR3_SDTIM3_OFFSET		(0x20)
#define DDR3_SDTIM4_OFFSET		(0x28)
#define DDR3_PMCTL_OFFSET		(0x38)
#define DDR3_ZQCFG_OFFSET		(0xC8)
#define DDR3_TMPALRT_OFFSET     (0xCC)
#define DDR3_DDRPHYC_OFFSET     (0xE4)
#define DDR3_ECC_CTRL_OFFSET    (0x110)


#define IODDRM_MASK            (0x00000180)
#define ZCKSEL_MASK            (0x01800000)
#define CL_MASK				   (0x00000072)
#define WR_MASK				   (0x00000E00)
#define BL_MASK				   (0x00000003)
#define RRMODE_MASK            (0x00040000)
#define UDIMM_MASK             (0x20000000)
#define BYTEMASK_MASK          (0x0000FC00)
#define MPRDQ_MASK             (0x00000080)
#define PDQ_MASK               (0x00000070)
#define NOSRA_MASK             (0x08000000)
#define ECC_MASK               (0x00000001)

typedef struct aud_ddr3_phy_config {
	uint32_t pllcr;
	uint32_t pgcr1_mask;
	uint32_t pgcr1_val;
	uint32_t ptr0;
	uint32_t ptr1;
	uint32_t ptr2;
	uint32_t ptr3;
	uint32_t ptr4;
	uint32_t dcr_mask;
	uint32_t dcr_val;
	uint32_t dtpr0;
	uint32_t dtpr1;
	uint32_t dtpr2;
	uint32_t mr0;
	uint32_t mr1;
	uint32_t mr2;
	uint32_t dtcr;
	uint32_t pgcr2;
	uint32_t zq0cr1;
	uint32_t zq1cr1;
	uint32_t zq2cr1;
	uint32_t pir_v1;
	uint32_t pir_v2;
	uint32_t ecc_ctl;
} aud_ddr3_phy_config;

typedef struct aud_ddr3_emif_config {
	uint32_t sdcfg;
	uint32_t sdtim1;
	uint32_t sdtim2;
	uint32_t sdtim3;
	uint32_t sdtim4;
	uint32_t zqcfg;
	uint32_t sdrfc;
} aud_ddr3_emif_config;

#define read_reg(a)          (*(volatile uint32_t *)(a))
#define write_reg(v,a)       (*(volatile uint32_t *)(a) = (v))


/********************************************************************************************
 * 					Function Prototypes														*
 *******************************************************************************************/
#if (AUD_SEMLOCK_IN)
#define AUD_PLIBSPILOCK() Osal_platformSpiCsEnter();
#define AUD_PLIBSPIRELEASE() Osal_platformSpiCsExit ();
#else
#define AUD_PLIBSPILOCK()
#define AUD_PLIBSPIRELEASE()
#endif

/* Function prototypes that don't live anywhere else */
extern void configSerdes();
extern void Init_SGMII(uint32_t macport);
extern void PowerUpDomains (void);
extern void xmc_setup();

CSL_Status  DDR3Init(void);
int32_t readSPD(uint8_t uchEepromI2cAddress,uint8_t buf[],uint8_t i2cportnumber);
int32_t init_ddr3param(uint8_t*);

#ifdef __cplusplus
}
#endif

#endif /* _AUD_INTERNAL_H_ */
