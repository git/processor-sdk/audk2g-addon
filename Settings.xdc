

module Settings
{
    /*! This is the AUD Version */
    config string audlldVersionString = "01.00.00.00";

    /*! This variable is to control the device type selection.
     * By default this variable is set to NULL.
     * 
     * To use AUD for the selected device, add the following lines to config
     * file and set the deviceType correctly:
     * 
     *      var aud = xdc.useModule ('ti.addon.aud.Settings');
     *      aud.socType = "k2g";
     * 
     * If this is not set, then hyperlink will use device independent
     * library where user must supply compiled aud_device.obj
     */
    metaonly config string socType = "";
    /*! Backwards compatible version of socType w/ keystone 2 */
    metaonly config string deviceType = "";

    /*! This flag is used to indicate whether or not the benchmarking code
     * (defined in the profilingHooks class) will be used in the project.
     * Note that a separate library has been compiled and will be used
     * ($NAME).profiling.a($SUFFIX). This is set in the *.cfg file.
     */
    config Bool enableProfiling = false;
	
    /*! This variable is to control the device library type selection.
     * By default this variable is set to release.
     * 
     * To use CSL to use the debug/release library, add the following lines to config
     * file and set the library profile accordingly:
     * 
     *      var Uart Settings = xdc.useModule ('ti.Uart.Settings');
     *      UartSettings.libProfile = "debug";
     * 
     */
    metaonly config string libProfile = "release";	

}



