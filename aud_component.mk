#*******************************************************************************
#                                                                              *
# Copyright (c) 2016 Texas Instruments Incorporated - http://www.ti.com/       *
#                        ALL RIGHTS RESERVED                                   *
#                                                                              *
#*******************************************************************************

# File: aud_component.mk
#       This file is component include make file of aud addon library.
# List of variables set in this file and their purpose:
# <mod>_RELPATH        - This is the relative path of the module, typically from
#                        top-level directory of the package
# <mod>_PATH           - This is the absolute path of the module. It derives from
#                        absolute path of the top-level directory (set in env.mk)
#                        and relative path set above
# <mod>_INCLUDE        - This is the path that has interface header files of the
#                        module. This can be multiple directories (space separated)
# <mod>_PKG_LIST       - Names of the modules (and sub-modules) that are a part
#                        part of this module, including itself.
# <mod>_BOARD_DEPENDENCY - "yes": means the code for this module depends on
#                             board and the compiled obj/lib has to be kept
#                             under <board> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no board dependent code and hence
#                             the obj/libs are not kept under <board> dir.
# <mod>_CORE_DEPENDENCY     - "yes": means the code for this module depends on
#                             core and the compiled obj/rtos has to be kept
#                             under <core> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no core dependent code and hence
#                             the obj/rtoss are not kept under <core> dir.
# <mod>_APP_STAGE_FILES     - List of source files that belongs to the module
#                             <mod>, but that needs to be compiled at application
#                             build stage (in the context of the app). This is
#                             primarily for link time configurations or if the
#                             source file is dependent on options/defines that are
#                             application dependent. This can be left blank or
#                             not defined at all, in which case, it means there
#                             no source files in the module <mod> that are required
#                             to be compiled in the application build stage.
#
ifeq ($(aud_component_make_include), )

# under other list
aud_BOARDLIST       = evmK2G
aud_SOCLIST         = k2g
aud_k2g_CORELIST    = c66x    #a15_0

############################
# aud package
# List of components included under aud rtos
# The components included here are built and will be part of aud rtos
############################
aud_LIB_LIST = aud
aud_EXAMPLE_LIST =

#
# AUD Modules
#

# AUD LIB
aud_COMP_LIST = aud
aud_RELPATH = ti/addon/aud
aud_PATH = $(PDK_AUD_COMP_PATH)
aud_LIBNAME = ti.addon.aud
export aud_LIBNAME
aud_LIBPATH = $(aud_PATH)/lib
export aud_LIBPATH
aud_OBJPATH = $(aud_RELPATH)/aud
export aud_OBJPATH
aud_MAKEFILE = -f build/makefile.mk
export aud_MAKEFILE
aud_BOARD_DEPENDENCY = no
aud_CORE_DEPENDENCY = no
aud_SOC_DEPENDENCY = yes
export aud_COMP_LIST
export aud_BOARD_DEPENDENCY
export aud_CORE_DEPENDENCY
export aud_SOC_DEPENDENCY
aud_PKG_LIST = aud
export aud_PKG_LIST
aud_INCLUDE = $(aud_PATH)
export aud_SOCLIST
export aud_$(SOC)_CORELIST

#
# AUD Examples
#

# AUD audio loopback app
aud_audio_loopback_app_COMP_LIST = aud_audio_loopback_app
aud_audio_loopback_app_RELPATH = ti/addon/aud/test/analog/loopback/build/make
aud_audio_loopback_app_PATH = $(PDK_AUD_COMP_PATH)/test/analog/loopback/build/make
aud_audio_loopback_app_BOARD_DEPENDENCY = yes
aud_audio_loopback_app_CORE_DEPENDENCY = no
aud_audio_loopback_app_XDC_CONFIGURO = no
export aud_audio_loopback_app_COMP_LIST
export aud_audio_loopback_app_BOARD_DEPENDENCY
export aud_audio_loopback_app_CORE_DEPENDENCY
export aud_audio_loopback_app_XDC_CONFIGURO
aud_audio_loopback_app_PKG_LIST = aud_audio_loopback_app
aud_audio_loopback_app_INCLUDE = $(aud_audio_loopback_app_PATH) $(PDK_AUD_COMP_PATH)/include
aud_audio_loopback_app_SOCLIST = k2g
export aud_audio_loopback_app_SOCLIST
aud_audio_loopback_app_$(SOC)_CORELIST = $(aud_$(SOC)_CORELIST)
export aud_audio_loopback_app_$(SOC)_CORELIST
aud_EXAMPLE_LIST += aud_audio_loopback_app

export aud_LIB_LIST
export aud_EXAMPLE_LIST

aud_component_make_include := 1
endif
