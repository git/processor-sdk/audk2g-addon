/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      main.c
 *
 * \brief     Audio DC analog test main file
 *
 *  Audio analog test verifies the functionality of ADC and DAC available on
 *  audio daughter card. During this test, data received from ADC will be
 *  sent to DAC. There are two instances of DAC and ADC available on
 *  audio daughter card. One ADC supports 4 analog channels and one DAC
 *  supports 8 analog channels. Data received from one DAC shall be sent
 *  to both the ADCs making 8 channel Rx/Tx. Each DAC should be validated
 *  separately.
 *
 */

#include "analog_test.h"
#include "board.h"

#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>

/* ADC default configuration parameters */
AudAdcConfig adcCfg =
{
	80,                  /* ADC gain */
	AUD_ADC_INL_SE_VINL1,    /* Left input mux for ADC1L */
	AUD_ADC_INL_SE_VINL2,    /* Left input mux for ADC2L */
	AUD_ADC_INR_SE_VINR1,    /* Right input mux for ADC1R */
	AUD_ADC_INR_SE_VINR2,    /* Right input mux for ADC2R */
	AUD_ADC_RX_WLEN_24BIT,   /* ADC word length */
	AUD_ADC_DATA_FORMAT_I2S, /* ADC data format */
	0
};

/* DAC default configuration parameters */
AudDacConfig  dacCfg =
{
	AUD_DAC_AMUTE_CTRL_SCKI_LOST,   /* Amute event */
	0,                          /* Amute control */
	AUD_DAC_SAMPLING_MODE_AUTO,     /* Sampling mode */
	AUD_DAC_DATA_FORMAT_I2S,        /* Data format */
	0,                          /* Soft mute control */
	AUD_DAC_ATTENUATION_WIDE_RANGE, /* Attenuation mode */
	AUD_DAC_DEEMP_44KHZ,            /* De-emph control */
	80                          /* Volume */
};

char write_buffer[MAX_WRITE_LEN];
void aud_write(const char *fmt, ... )
{
	va_list 	arg_ptr;
	uint32_t	length;

	/* Initial platform_write to temporary buffer.. at least try some sort of sanity check so we don't write all over
	 * memory if the print is too large.
	 */
	if (strlen(fmt) > MAX_WRITE_LEN) {return;}

	va_start( arg_ptr, fmt );
	length = vsprintf( (char *)write_buffer, fmt, arg_ptr );
	va_end( arg_ptr );

	printf( "%s", write_buffer );
	//fflush(stdout);

	return;
}

void McaspDevice_init(void);

/**
 *  \brief    Initializes platform specific modules
 *
 *  This function initializes the modules like PLL, DDR, I2C etc
 *  which are required for audio processing. Need to call this function
 *  before doing any HW related operations.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS initPlatform(void)
{
	Board_STATUS status = BOARD_SOK;
	Board_initCfg arg = BOARD_INIT_PINMUX_CONFIG | BOARD_INIT_MODULE_CLOCK | BOARD_INIT_UART_STDIO;
	status = Board_init(arg);

	// I2C init
    /* Initialize all the I2C ports - Needed for different I2C devices on the board */
	evmI2CInit(I2C_PORT_0);
	evmI2CInit(I2C_PORT_1);
	evmI2CInit(I2C_PORT_2);

	///pinMuxSetMode(125, PADCONFIG_MUX_MODE_QUATERNARY);
	///gpioSetDirection(GPIO_PORT_1, 54, GPIO_OUT);
	///gpioSetOutput(GPIO_PORT_1, 54);

	/* Configure platform log messages to standard printf */
    ///Aud_write_configure(Aud_WRITE_UART);

    /* Initialize UART */
    ///Aud_uart_init();
    ///Aud_uart_set_baudrate(115200);

    return(status);
}

/**
 *  \brief    Audio analog test main function
 *
 *  \return    none
 */
void main (void)
{
	Aud_STATUS status;

	status = initPlatform();
	if(status != Aud_EOK)
	{
		aud_write("Platform Init Failed!\n");
		testRet(1);
	}

	aud_delay(10000);



	aud_write("\n******************************************\n");
	aud_write(  "      Audio DC Analog Loopback Test       \n");
    aud_write(  "******************************************\n");
	UART_printf("\n******************************************\n");
	UART_printf(  "      Audio DC Analog Loopback Test       \n");
    UART_printf(  "******************************************\n");

#ifdef AUDIO_DAC0_TEST
    aud_write("\nTest Verifies ADC and DAC Channels 0 to 3\n");
	aud_write("Test Runs in Audio Loopback Mode\n");
	aud_write("Confirm that ADC Input Audio is Played at DAC Channels 0 to 3\n");
	aud_write("\nAudio Connection Details\n");

    UART_printf("\nTest Verifies ADC and DAC Channels 0 to 3\n");
	UART_printf("Test Runs in Audio Loopback Mode\n");
	UART_printf("Confirm that ADC Input Audio is Played at DAC Channels 0 to 3\n");
	UART_printf("\nAudio Connection Details\n");
#ifdef K2G_ALPHA_EVM_BUILD
	aud_write("**************************************\n");
	aud_write("ADC1_STEREO_IN1 ==> DAC0_STEREO_OUT1\n");
	aud_write("ADC1_STEREO_IN2 ==> DAC0_STEREO_OUT2\n");
	aud_write("ADC2_STEREO_IN1 ==> DAC0_STEREO_OUT3\n");
	aud_write("ADC2_STEREO_IN2 ==> DAC0_STEREO_OUT4\n");
	aud_write("**************************************\n");

	UART_printf("**************************************\n");
	UART_printf("ADC1_STEREO_IN1 ==> DAC0_STEREO_OUT1\n");
	UART_printf("ADC1_STEREO_IN2 ==> DAC0_STEREO_OUT2\n");
	UART_printf("ADC2_STEREO_IN1 ==> DAC0_STEREO_OUT3\n");
	UART_printf("ADC2_STEREO_IN2 ==> DAC0_STEREO_OUT4\n");
	UART_printf("**************************************\n");
#else
	aud_write("**************************************\n");
	aud_write("ADC_IN 0 ==> DAC_OUT 0\n");
	aud_write("ADC_IN 1 ==> DAC_OUT 1\n");
	aud_write("ADC_IN 2 ==> DAC_OUT 2\n");
	aud_write("ADC_IN 3 ==> DAC_OUT 3\n");
	aud_write("**************************************\n");
	
	UART_printf("**************************************\n");
	UART_printf("ADC_IN 0 ==> DAC_OUT 0\n");
	UART_printf("ADC_IN 1 ==> DAC_OUT 1\n");
	UART_printf("ADC_IN 2 ==> DAC_OUT 2\n");
	UART_printf("ADC_IN 3 ==> DAC_OUT 3\n");
	UART_printf("**************************************\n");
#endif

#else

    aud_write("\nTest Verifies ADC Channels 0 to 3 and DAC Channels 4 to 7\n");
	aud_write("Test Runs in Audio Loopback Mode\n");
	aud_write("Confirm that ADC Input Audio is Played at DAC Channels 4 to 7\n");
	aud_write("\nAudio Connection Details\n");

    UART_printf("\nTest Verifies ADC Channels 0 to 3 and DAC Channels 4 to 7\n");
	UART_printf("Test Runs in Audio Loopback Mode\n");
	UART_printf("Confirm that ADC Input Audio is Played at DAC Channels 4 to 7\n");
	UART_printf("\nAudio Connection Details\n");	
#ifdef K2G_ALPHA_EVM_BUILD
	aud_write("**************************************\n");
	aud_write("ADC1_STEREO_IN1 ==> DAC1_STEREO_OUT1\n");
	aud_write("ADC1_STEREO_IN2 ==> DAC1_STEREO_OUT2\n");
	aud_write("ADC2_STEREO_IN1 ==> DAC1_STEREO_OUT3\n");
	aud_write("ADC2_STEREO_IN2 ==> DAC1_STEREO_OUT4\n");
	aud_write("**************************************\n");

	UART_printf("**************************************\n");
	UART_printf("ADC1_STEREO_IN1 ==> DAC1_STEREO_OUT1\n");
	UART_printf("ADC1_STEREO_IN2 ==> DAC1_STEREO_OUT2\n");
	UART_printf("ADC2_STEREO_IN1 ==> DAC1_STEREO_OUT3\n");
	UART_printf("ADC2_STEREO_IN2 ==> DAC1_STEREO_OUT4\n");
	UART_printf("**************************************\n");	
#else
	aud_write("**************************************\n");
	aud_write("ADC_IN 0 ==> DAC_OUT 4\n");
	aud_write("ADC_IN 1 ==> DAC_OUT 5\n");
	aud_write("ADC_IN 2 ==> DAC_OUT 6\n");
	aud_write("ADC_IN 3 ==> DAC_OUT 7\n");
	aud_write("**************************************\n");

	UART_printf("**************************************\n");
	UART_printf("ADC_IN 0 ==> DAC_OUT 4\n");
	UART_printf("ADC_IN 1 ==> DAC_OUT 5\n");
	UART_printf("ADC_IN 2 ==> DAC_OUT 6\n");
	UART_printf("ADC_IN 3 ==> DAC_OUT 7\n");
	UART_printf("**************************************\n");
#endif

#endif

	/* Initialize McASP HW details */
	McaspDevice_init();

	/* Configure eDMA module */
	status = eDmaConfig();
	if(status != Aud_EOK)
	{
		aud_write("eDMA Configuration Failed!\n");
		testRet(1);
	}

	/* Initialize common audio configurations */
	status = (Aud_STATUS)aud_AudioInit();
	if(status != Aud_EOK)
	{
		aud_write("Audio Init Failed!\n");
		testRet(1);
	}

	/* Initialize Audio ADC module */
	status = audioAdcConfig(AUD_ADC_DEVICE_ALL, &adcCfg);
	if(status != Aud_EOK)
	{
		aud_write("Audio ADC Configuration Failed!\n");
		testRet(1);
	}

	/* Initialize McASP module */
	status = mcaspAudioConfig();
	if(status != Aud_EOK)
	{
		aud_write("McASP Configuration Failed!\n");
		testRet(1);
	}

	/* Start BIOS execution */
	BIOS_start();
}

/**
 *  \brief    Configures audio DAC module
 *
 *  \return    none
 */
void configAudioDAC(void)
{
	Aud_STATUS status;

	/* Initialize Audio DAC module */
	status = audioDacConfig(AUD_DAC_DEVICE_ALL, &dacCfg);
	if(status != Aud_EOK)
	{
		aud_write("Audio DAC Configuration Failed!\n");
		testRet(1);
	}
}

/* Nothing past this point */
