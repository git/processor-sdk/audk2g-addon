/*
 * Copyright (c) 2015 - 2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      audio_dc_config.c
 *
 * \brief     Configures Audio daughter card HW modules
 *
 */

#include "audio_dc_cfg.h"
#include "aud_audio.h"

/**
 *  \brief    Initializes ADC module
 *
 *  This function initializes and configures the ADC modules
 *  on audio daughter card
 *
 *  \param     devId  [IN]  ADC Device Id
 *  \param     config [IN]  ADC configuration parameters
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS audioAdcConfig(AudAdcDevId  devId, AudAdcConfig *config)
{
	Aud_STATUS status;

	if(config == NULL)
	{
		IFPRINT(aud_write("Invalid Inputs\n"));
		return (Aud_EINVALID);
	}

	/* Initialize all the HW instances of ADC */
	//status = platformAudioAdcInit(devId);
	status = (Aud_STATUS)aud_AudioAdcInit((AudAdcDevId)devId);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformaudioAdcConfig Failed\n"));
		return (status);
	}

	/* Set ADC channel gain */
	//status = platformAudioAdcSetGain(devId, ADC_CH_ALL, config->gain);
    status = (Aud_STATUS)aud_AudioAdcSetGain((AudAdcDevId)devId, (AudAdcChanId)AUD_ADC_CH_ALL, config->gain);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformAudioAdcSetGain Failed\n"));
		return (status);
	}

	/* Configure Left input mux for ADC1L */
	//status = platformAudioAdcSetLeftInputMux(devId, ADC_CH1_LEFT, config->adc1LMux);
    status = (Aud_STATUS)aud_AudioAdcSetLeftInputMux((AudAdcDevId)devId, (AudAdcChanId)AUD_ADC_CH1_LEFT, (AudAdcLeftInputMux)(config->adc1LMux));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformAudioAdcSetLeftInputMux Failed\n"));
		return (status);
	}

	/* Configure Left input mux for ADC2L*/
	//status = platformAudioAdcSetLeftInputMux(devId, ADC_CH2_LEFT, config->adc2LMux);
	status = (Aud_STATUS)aud_AudioAdcSetLeftInputMux((AudAdcDevId)devId, (AudAdcChanId)AUD_ADC_CH2_LEFT, (AudAdcLeftInputMux)(config->adc2LMux));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformAudioAdcSetLeftInputMux Failed\n"));
		return (status);
	}

	/* Configure Right input mux for ADC1R */
	//status = platformAudioAdcSetRightInputMux(devId, ADC_CH1_RIGHT, config->adc1RMux);
    status = (Aud_STATUS)aud_AudioAdcSetRightInputMux((AudAdcDevId)devId, (AudAdcChanId)AUD_ADC_CH1_RIGHT, (AudAdcRightInputMux)(config->adc1RMux));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformAudioAdcSetRightInputMux Failed\n"));
		return (status);
	}

	/* Configure Right input mux for ADC2R */
	//status = platformAudioAdcSetRightInputMux(devId, ADC_CH2_RIGHT, config->adc2RMux);
    status = (Aud_STATUS)aud_AudioAdcSetRightInputMux((AudAdcDevId)devId, (AudAdcChanId)AUD_ADC_CH2_RIGHT, (AudAdcRightInputMux)(config->adc2RMux));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformAudioAdcSetRightInputMux Failed\n"));
		return (status);
	}

	/* Configure audio data format */
    //status = platformAudioAdcDataConfig(devId, config->wlen, config->format);
	status = (Aud_STATUS)aud_AudioAdcDataConfig((AudAdcDevId)devId, (AudAdcRxWordLen)(config->wlen), (AudAdcDataFormat)(config->format));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformAudioAdcDataConfig Failed\n"));
		return (status);
	}

	/* Configure all the interrupts */
	//status = platformAudioAdcConfigIntr(devId, ADC_INTR_ALL, config->intEnable);
    status = (Aud_STATUS)aud_AudioAdcConfigIntr((AudAdcDevId)devId, (AudAdcIntr)AUD_ADC_INTR_ALL, config->intEnable);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioAdcConfig : platformAudioAdcConfigIntr Failed\n"));
		return (status);
	}

	return (status);
}

/**
 *  \brief    Initializes DAC module
 *
 *  This function initializes and configures the DAC modules
 *  on audio daughter card
 *
 *  \param     devId  [IN]  DAC Device Id
 *  \param     config [IN]  DAC configuration parameters
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS audioDacConfig(AudDacDevId devId, AudDacConfig *config)
{
	Aud_STATUS status;

	if(config == NULL)
	{
		IFPRINT(aud_write("Invalid Inputs\n"));
		return (Aud_EINVALID);
	}

	/* Initialize Audio DAC */
	//status = platformAudioDacInit(devId);
    status = (Aud_STATUS)aud_AudioDacInit((AudDacDevId)devId);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformaudioDacConfig Failed\n"));
		return (status);
	}

	/* Configure AMUTE control event */
	//status = platformAudioDacAmuteCtrl(devId, config->amuteCtrl, config->amuteEnable);
    status = (Aud_STATUS)aud_AudioDacAmuteCtrl((AudDacDevId)devId, (AudDacAmuteCtrl)(config->amuteCtrl), config->amuteEnable);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformAudioDacAmuteCtrl Failed\n"));
		return (status);
	}

	/* Set sampling mode */
	//status = platformAudioDacSetSamplingMode(devId, config->samplingMode);
    status = (Aud_STATUS)aud_AudioDacSetSamplingMode((AudDacDevId)devId, (AudDacSamplingMode)(config->samplingMode));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformAudioDacSetSamplingMode Failed\n"));
		return (status);
	}

	/* Set data format */
	//status = platformAudioDacSetDataFormat(devId, config->dataFormat);
    status = (Aud_STATUS)aud_AudioDacSetDataFormat((AudDacDevId)devId, (AudDacDataFormat)(config->dataFormat));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformAudioDacSetDataFormat Failed\n"));
		return (status);
	}

	/* Enable soft mute control */
	//status = platformAudioDacSoftMuteCtrl(devId, DAC_CHAN_ALL, config->softMuteEnable);
    status = (Aud_STATUS)aud_AudioDacSoftMuteCtrl((AudDacDevId)devId, (AudDacChanId)AUD_DAC_CHAN_ALL, config->softMuteEnable);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformAudioDacSoftMuteCtrl Failed\n"));
		return (status);
	}

	/* Set attenuation mode */
	//status = platformAudioDacSetAttnMode(devId, config->attnMode);
    status = (Aud_STATUS)aud_AudioDacSetAttnMode((AudDacDevId)devId, (AudDacAttnMode)(config->attnMode));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformAudioDacSetAttnMode Failed\n"));
		return (status);
	}

	/* Set De-emphasis control */
	//status = platformAudioDacDeempCtrl(devId, config->deempCtrl);
    status = (Aud_STATUS)aud_AudioDacDeempCtrl((AudDacDevId)devId, (AudDacDeempCtrl)(config->deempCtrl));
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformAudioDacDeempCtrl Failed\n"));
		return (status);
	}

	/* Set DAC volume */
	//status = platformAudioDacSetVolume(devId, DAC_CHAN_ALL, config->volume);
    status = (Aud_STATUS)aud_AudioDacSetVolume((AudDacDevId)devId, (AudDacChanId)AUD_DAC_CHAN_ALL, config->volume);
	if(status != Aud_EOK)
	{
		IFPRINT(aud_write("audioDacConfig : platformAudioDacSetVolume Failed\n"));
		return (status);
	}

	return (status);
}

/* Nothing past this point */
