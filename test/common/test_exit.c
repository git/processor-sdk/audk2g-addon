/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
*
* \file test_exit.c
*
* \brief This file implements the routines used to exit the diagnostic tests
*        which are loaded dynamically from SD card
*
******************************************************************************/

/**
 *  \brief Below are the shared memory locations used by diagnostic test
 *         framework standalone test modules
 */
/** Memory location to store test exit address */
#define DIAG_TEST_EXIT_ADDR   (0x0c0e0100)
/** Memory location to store test pass count */
#define DIAG_TEST_PASS_PTR    (0x0c0e0104)
/** Memory location to store test fail count */
#define DIAG_TEST_FAIL_PTR    (0x0c0e0108)

/**< Function pointer for test exit */
void (*pFnTestExit)(void);

/**
 *  \brief    Function to exit the test
 *
 *  This function should be called by standalone diagnostic
 *  tests at the end of the test to return to diagnostic test framework
 *
 *  \param result [IN]  Test result
 *                      0 - Test Pass
 *                      1 - Test Fail
 *
 */
void testExit(int result)
{
	unsigned int exitPoint = *(unsigned int *)DIAG_TEST_EXIT_ADDR;
	unsigned int *pTestPass = (unsigned int *)(*(unsigned int *)DIAG_TEST_PASS_PTR);
	unsigned int *pTestFail = (unsigned int *)(*(unsigned int *)DIAG_TEST_FAIL_PTR);;

	if(result)
	{
		(*pTestFail)++;
	}
	else
	{
		(*pTestPass)++;
	}

	/* Jump to test exit function in diagnostic test framework */
	pFnTestExit = (void (*)(void))exitPoint;
	pFnTestExit();
}

/* Nothing past this point */
