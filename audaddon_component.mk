#
# Copyright (c) 2016, Texas Instruments Incorporated
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# *  Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# *  Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# *  Neither the name of Texas Instruments Incorporated nor the names of
#    its contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
# EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

# File: audaddon_component.mk
#       This file is component include make file of aud-addon library.
# List of variables set in this file and their purpose:
# <mod>_RELPATH        - This is the relative path of the module, typically from
#                        top-level directory of the package
# <mod>_PATH           - This is the absolute path of the module. It derives from
#                        absolute path of the top-level directory (set in env.mk)
#                        and relative path set above
# <mod>_INCLUDE        - This is the path that has interface header files of the
#                        module. This can be multiple directories (space separated)
# <mod>_PKG_LIST       - Names of the modules (and sub-modules) that are a part
#                        part of this module, including itself.
# <mod>_BOARD_DEPENDENCY - "yes": means the code for this module depends on
#                             board and the compiled obj/lib has to be kept
#                             under <board> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no board dependent code and hence
#                             the obj/libs are not kept under <board> dir.
# <mod>_CORE_DEPENDENCY     - "yes": means the code for this module depends on
#                             core and the compiled obj/lib has to be kept
#                             under <core> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no core dependent code and hence
#                             the obj/libs are not kept under <core> dir.
# <mod>_APP_STAGE_FILES     - List of source files that belongs to the module
#                             <mod>, but that needs to be compiled at application
#                             build stage (in the context of the app). This is
#                             primarily for link time configurations or if the
#                             source file is dependent on options/defines that are
#                             application dependent. This can be left blank or
#                             not defined at all, in which case, it means there
#                             no source files in the module <mod> that are required
#                             to be compiled in the application build stage.
#
ifeq ($(aud_component_make_include), )

# under other list
drvaud_BOARDLIST       = evmK2G
drvaud_SOCLIST         = k2g
drvaud_k2g_CORELIST    = c66x a15_0


############################
# uart package
# List of components included under uart lib
# The components included here are built and will be part of uart lib
############################
aud_LIB_LIST = aud aud_profile aud_indp aud_profile_indp
drvaud_LIB_LIST = $(aud_LIB_LIST)

#
# AUD-ADDON Modules
#

# AUD-ADDON LIB
aud_COMP_LIST = aud
aud_RELPATH = ti/addon/aud
aud_PATH = $(PDK_AUD_COMP_PATH)
aud_LIBNAME = ti.addon.aud
export aud_LIBNAME
aud_LIBPATH = $(aud_PATH)/lib
export aud_LIBPATH
aud_OBJPATH = $(aud_RELPATH)/aud
export aud_OBJPATH
aud_MAKEFILE = -f build/makefile.mk
export aud_MAKEFILE
aud_BOARD_DEPENDENCY = no
aud_CORE_DEPENDENCY = no
aud_SOC_DEPENDENCY = yes
export aud_COMP_LIST
export aud_BOARD_DEPENDENCY
export aud_CORE_DEPENDENCY
export aud_SOC_DEPENDENCY
aud_PKG_LIST = aud
export aud_PKG_LIST
aud_INCLUDE = $(aud_PATH)
aud_SOCLIST = $(drvaud_SOCLIST)
export aud_SOCLIST
aud_$(SOC)_CORELIST = $(drvaud_$(SOC)_CORELIST)
export aud_$(SOC)_CORELIST

# AUD-ADDON LIB DEVICE INDEPENDENT
aud_indp_COMP_LIST = aud_indp
aud_indp_RELPATH = ti/addon/aud
aud_indp_PATH = $(PDK_AUD_COMP_PATH)
aud_indp_LIBNAME = ti.addon.aud
export aud_indp_LIBNAME
aud_indp_LIBPATH = $(aud_indp_PATH)/lib
export aud_indp_LIBPATH
aud_indp_OBJPATH = $(aud_indp_RELPATH)/aud_indp
export aud_indp_OBJPATH
aud_indp_MAKEFILE = -f build/makefile_indp.mk
export aud_indp_MAKEFILE
aud_indp_BOARD_DEPENDENCY = no
aud_indp_CORE_DEPENDENCY = no
aud_indp_SOC_DEPENDENCY = no
export aud_indp_COMP_LIST
export aud_indp_BOARD_DEPENDENCY
export aud_indp_CORE_DEPENDENCY
export aud_indp_SOC_DEPENDENCY
aud_indp_PKG_LIST = aud_indp
aud_indp_INCLUDE = $(aud_indp_PATH)
aud_indp_SOCLIST = $(drvaud_SOCLIST)
export aud_indp_SOCLIST
aud_indp_$(SOC)_CORELIST = $(drvaud_$(SOC)_CORELIST)
export aud_indp_$(SOC)_CORELIST

# AUD-ADDON PROFILING SOC LIB
aud_profile_COMP_LIST = aud_profile
aud_profile_RELPATH = ti/addon/aud
aud_profile_PATH = $(PDK_AUD_COMP_PATH)
aud_profile_LIBNAME = ti.addon.aud.profiling
export aud_profile_LIBNAME
aud_profile_LIBPATH = $(aud_profile_PATH)/lib
export aud_profile_LIBPATH
aud_profile_OBJPATH = $(aud_profile_RELPATH)/aud_profile
export aud_profile_OBJPATH
aud_profile_MAKEFILE = -f build/makefile_profile.mk
export aud_profile_MAKEFILE
aud_profile_BOARD_DEPENDENCY = no
aud_profile_CORE_DEPENDENCY = no
aud_profile_SOC_DEPENDENCY = yes
export aud_profile_COMP_LIST
export aud_profile_BOARD_DEPENDENCY
export aud_profile_CORE_DEPENDENCY
export aud_profile_SOC_DEPENDENCY
aud_profile_PKG_LIST = aud_profile
aud_profile_INCLUDE = $(aud_profile_PATH)
aud_profile_SOCLIST = $(drvaud_SOCLIST)
export aud_profile_SOCLIST
aud_profile_$(SOC)_CORELIST = $(drvaud_$(SOC)_CORELIST)
export aud_profile_$(SOC)_CORELIST

# AUD-ADDON PROFILING SOC INDEPENDENT LIB
aud_profile_indp_COMP_LIST = aud_profile_indp
aud_profile_indp_RELPATH = ti/addon/aud
aud_profile_indp_PATH = $(PDK_AUD_COMP_PATH)
aud_profile_indp_LIBNAME = ti.addon.aud.profiling
export aud_profile_indp_LIBNAME
aud_profile_indp_LIBPATH = $(aud_profile_indp_PATH)/lib
export aud_profile_indp_LIBPATH
aud_profile_indp_OBJPATH = $(aud_profile_indp_RELPATH)/aud_profile_indp
export aud_profile_indp_OBJPATH
aud_profile_indp_MAKEFILE = -f build/makefile_profile_indp.mk
export aud_profile_indp_MAKEFILE
aud_profile_indp_BOARD_DEPENDENCY = no
aud_profile_indp_CORE_DEPENDENCY = no
aud_profile_indp_SOC_DEPENDENCY = no
export aud_profile_indp_COMP_LIST
export aud_profile_indp_BOARD_DEPENDENCY
export aud_profile_indp_CORE_DEPENDENCY
export aud_profile_indp_SOC_DEPENDENCY
aud_profile_indp_PKG_LIST = aud_profile_indp
aud_profile_indp_INCLUDE = $(aud_profile_indp_PATH)
aud_profile_indp_SOCLIST = $(drvaud_SOCLIST)
export aud_profile_indp_SOCLIST
aud_profile_indp_$(SOC)_CORELIST = $(drvaud_$(SOC)_CORELIST)
export aud_profile_indp_$(SOC)_CORELIST

export drvaud_LIB_LIST
export aud_LIB_LIST
export aud_EXAMPLE_LIST

aud_component_make_include := 1
endif
