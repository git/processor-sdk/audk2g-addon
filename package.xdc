/******************************************************************************
 * FILE PURPOSE: Package specification file 
 ******************************************************************************
 * FILE NAME: package.xdc
 *
 * DESCRIPTION: 
 *  This file contains the package specification for the Board library
 *
 * Copyright (C) 2011-2018, Texas Instruments, Inc.
 *****************************************************************************/

package ti.addon.aud[1,9,0,0] {
    module Settings;
}
