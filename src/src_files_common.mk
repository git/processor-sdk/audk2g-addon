#
# This file is the makefile for building GPIO RTOS library.
#
#   Copyright (c) Texas Instruments Incorporated 2016
#

SRCDIR = . src
INCDIR = . src

# Common source files across all platforms and cores
SRCS_COMMON += aud.c
SRCS_COMMON += aud_dc_adc.c
SRCS_COMMON += aud_dc_dac.c
SRCS_COMMON += evmc66x_pinmux.c
SRCS_COMMON += evmc66x_i2c.c
SRCS_COMMON += evmc66x_gpio.c

PACKAGE_SRCS_COMMON = makefile include/aud.h aud_component.mk \
                      src/aud.c \
                      src/aud_dc_adc.c \
                      src/aud_dc_dac.c \
                      src/evmc66x_pinmux.c \
                      src/evmc66x_i2c.c \
                      src/evmc66x_gpio.c \
	 	      build/makefile.mk \
	 	      src/src_files_common.mk
                      

