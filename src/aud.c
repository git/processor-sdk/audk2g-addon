/*
 * Copyright (c) 2015 - 2018, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      aud_audio.c
 *
 * \brief     Platform audio interface file.
 *
 * This file contains APIs for accessing DAC/ADC/DIR modules on audio daughter
 * card.
 *
 */

#include "../include/aud.h"
#include "../include/aud_audio.h"

#if (AUD_AUDIO)

/* DAC HW instance I2C slave address */
Uint8 aud_gDacI2cAddr[AUD_AUDIO_DAC_COUNT] = {AUD_AUDIO_DAC0_ADDR,
                                               AUD_AUDIO_DAC1_ADDR};
/* ADC HW instance I2C slave address */
Uint8 aud_gAdcI2cAddr[AUD_AUDIO_AUD_ADC_COUNT] = {AUD_AUDIO_ADC0_ADDR,
                                               AUD_AUDIO_ADC1_ADDR};

uint32_t aud_errno = 0;

/******************************************************************************
 * aud_delay
 ******************************************************************************/
#define K2G_C66X_MHZ (600)
Aud_STATUS aud_delay(uint32_t usecs)
{
	int32_t delayCount = (int32_t) usecs * K2G_C66X_MHZ;
	int32_t start_val  = (int32_t) CSL_chipReadTSCL();

	while (((int32_t)CSL_chipReadTSCL() - start_val) < delayCount);

	return Aud_EOK;
}
                                               
/**
 *  \brief    Initializes Audio module
 *
 *  This function configures the system level setup required for
 *  operation of the modules that are available on audio daughter card.
 *  This function shall be called before calling any other platform
 *  audio module functions.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioInit(void)
{
	uint8_t aud_padCfg;
	uint8_t aud_padMax;

	IFPRINT(aud_write("aud_AudioInit Called \n"));

	/* Configure McASP0 lines */
	/* MCASP0AMUTE, MCASP0ACLKR, MCASP0FSR, MCASP0AHCLKR
       MCASP0ACLKX, MCASP0FSX, MCASP0AHCLKX,
       MCASP0AXR[0:7] - PADCONFIG 169 to 183 */
    aud_padMax = 183;
	for (aud_padCfg = 169; aud_padCfg <= aud_padMax; aud_padCfg++)
	{
		aud_pinMuxSetMode(aud_padCfg, AUD_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		aud_pullSetMode(aud_padCfg, 0);
	}

	/* MCASP0AXR[12:15] - PADCONFIG 188 to 191 */
    aud_padMax = 191;
	for (aud_padCfg = 188; aud_padCfg <= aud_padMax; aud_padCfg++)
	{
		aud_pinMuxSetMode(aud_padCfg, AUD_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		aud_pullSetMode(aud_padCfg, 0);
	}


	/* Configure McASP1 lines */
	/* MCASP1ACLKR, MCASP1FSR, MCASP1AHCLKR - PADCONFIG 152 to 154 */
    aud_padMax = 154;
	for (aud_padCfg = 152; aud_padCfg <= aud_padMax; aud_padCfg++)
	{
		aud_pinMuxSetMode(aud_padCfg, AUD_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		aud_pullSetMode(aud_padCfg, 0);
	}

	/* MCASP1AHCLKX - PADCONFIG 157 */
	aud_pinMuxSetMode(157, AUD_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	aud_pullSetMode(157, 0);

	/* MCASP1AXR[0:3] - PADCONFIG 159 to 162 */
    aud_padMax = 162;
	for (aud_padCfg = 159; aud_padCfg <= aud_padMax; aud_padCfg++)
	{
		aud_pinMuxSetMode(aud_padCfg, AUD_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		aud_pullSetMode(aud_padCfg, 0);
	}

	/* MCASP1AXR9 - PADCONFIG 168 */
	aud_pinMuxSetMode(168, AUD_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	aud_pullSetMode(168, 0);


	/* Configure McASP2 lines */
	/* MCASP2AXR[1:5], MCASP2ACLKR, MCASP2FSR, MCASP2AHCLKR
       - PADCONFIG 140 to 147 */
    aud_padMax = 147;
	for (aud_padCfg = 140; aud_padCfg <= aud_padMax; aud_padCfg++)
	{
		aud_pinMuxSetMode(aud_padCfg, AUD_PADCONFIG_MUX_MODE_QUINARY);
		// set to weak pull down
		aud_pullSetMode(aud_padCfg, 0);
	}

	/* MCASP2FSX - PADCONFIG 149 */
	aud_pinMuxSetMode(149, AUD_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	aud_pullSetMode(149, 0);

	/* MCASP2ACLKX - PADCONFIG 151 */
	aud_pinMuxSetMode(151, AUD_PADCONFIG_MUX_MODE_QUINARY);
	// set to weak pull down
	aud_pullSetMode(151, 0);

	/* Configure GPIO for McASP_CLK_SEL - GPIO0 132 & PADCONFIG 163 */
	aud_pinMuxSetMode(163, AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SEL_GPIO, AUD_GPIO_OUT);
	/* Slect the clock source as DIR */
	aud_gpioSetOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SEL_GPIO);

	/* Configure GPIO for McASP_CLK_SEL# - GPIO0 101 & PADCONFIG 110 */
	aud_pinMuxSetMode(110, AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SELz_GPIO, AUD_GPIO_OUT);
	/* Slect the clock source as DIR */
	//aud_gpioClearOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SELz_GPIO);
	aud_gpioSetOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SELz_GPIO);  // Default configurations are set for McASP AHCLK driven by SoC

	/* Configure GPIO for PCM1690_RST# - GPIO1 10 & PADCONFIG 185 */
	aud_pinMuxSetMode(185, AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_1, AUD_AUDIO_PCM1690_RST_GPIO, AUD_GPIO_OUT);

	aud_gpioClearOutput(AUD_GPIO_PORT_1, AUD_AUDIO_PCM1690_RST_GPIO);
	aud_delay(1000);
	aud_gpioSetOutput(AUD_GPIO_PORT_1, AUD_AUDIO_PCM1690_RST_GPIO);

	/* Configure McASP AUXCLK source as AUDIO_OSCCLK  */
	hBootCfg->SERIALPORT_CLKCTL = 0;
	hBootCfg->OSC_CTL = 0x200;	//SW2:SW1 = 01 (15 - 30 MHz)

    return (Aud_EOK);
}

/**
 *  \brief    Resets audio DAC
 *
 *  This function toggles the GPIO signal connected to RST pin
 *  of DAC module to generate DAC reset.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioResetDac(void)
{
	aud_gpioClearOutput(AUD_GPIO_PORT_1, AUD_AUDIO_PCM1690_RST_GPIO);
	aud_delay(1000);
	aud_gpioSetOutput(AUD_GPIO_PORT_1, AUD_AUDIO_PCM1690_RST_GPIO);

	/* Configure McASP AUXCLK source as AUDIO_OSCCLK  */
	hBootCfg->SERIALPORT_CLKCTL = 0;

    return (Aud_EOK);
}

/**
 *  \brief    Configures audio clock source
 *
 *  McASP can receive clock from DIR module on daughter card or external
 *  I2S device to operate DAC and ADC modules. This function configures
 *  which clock source to use (DIR or I2S) for DAC and ADC operation
 *
 *  \param clkSrc    [IN]  Clock source selection
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioSelectClkSrc(AudAudioClkSrc clkSrc)
{
	IFPRINT(aud_write("aud_AudioSelectClkSrc Called \n"));

	/* Configure GPIO for McASP_CLK_SEL - GPIO0 132 & PADCONFIG 163 */
	aud_pinMuxSetMode(163, AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SEL_GPIO, AUD_GPIO_OUT);

	/* Configure GPIO for McASP_CLK_SEL# - GPIO0 101 & PADCONFIG 110 */
	aud_pinMuxSetMode(110, AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SELz_GPIO, AUD_GPIO_OUT);

	if(clkSrc == AUD_AUDIO_CLK_SRC_DIR)
	{
		aud_gpioSetOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SEL_GPIO);
		aud_gpioClearOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SELz_GPIO);
	}
	else if(clkSrc == AUD_AUDIO_CLK_SRC_I2S)
	{
		aud_gpioClearOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SEL_GPIO);
		aud_gpioSetOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SELz_GPIO);
	}
	else if(clkSrc == AUD_AUDIO_CLK_SRC_OSC)
	{
	   aud_gpioSetOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SEL_GPIO);
	   aud_gpioSetOutput(AUD_GPIO_PORT_0, AUD_AUDIO_CLK_SELz_GPIO);
	}
	else
	{
		IFPRINT(aud_write("aud_AudioSelectClkSrc : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	return (Aud_EOK);
}

#if (AUD_AUDIO_ADC)

/**
 *  \brief    Initializes ADC module
 *
 *  This function configures the system level setup required for ADC
 *  operation and initializes the ADC module with default values.
 *  This function should be called before calling any other ADC functions.
 *
 *  After executing this function, ADC module will be ready for audio
 *  processing with default configuration. Default ADC configurations
 *  can be changed using the other ADC APIs if required.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'AUD_ADC_DEVICE_ALL' to initialize
 *                        all the ADC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcInit(AudAdcDevId devId)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcInit Called \n"));

	if(!(devId <= AUD_ADC_DEVICE_ALL))
	{
		IFPRINT(aud_write("aud_AudioAdcInit : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xAdcInit(aud_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcInit : ADC Initialization Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_INIT;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Resets ADC module
 *
 *  This function resets all the ADC module registers to their
 *  HW default values.
 *
 *  \param devId    [IN]  Device ID of ADC HW instance
 *                        Use 'AUD_ADC_DEVICE_ALL' to reset
 *                        all the ADC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcReset(AudAdcDevId devId)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcReset Called \n"));

	if(!(devId <= AUD_ADC_DEVICE_ALL))
	{
		IFPRINT(aud_write("aud_AudioAdcReset : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_aud_pcm186xReset(aud_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcReset : ADC Reset Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures ADC gain value
 *
 *  Range of the gain is exposed as percentage by this API. Gain
 *  is indicated as percentage of maximum gain ranging from 0 to 100.
 *  0 indicates minimum gain and 100 indicates maximum gain.
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          Use AUD_ADC_CH_ALL to set gain for all the
 *                          ADC channels
 *
 *  \param gain       [IN]  Gain value; 0 to 100
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcSetGain(AudAdcDevId  devId,
                                        AudAdcChanId chanId,
                                        uint8_t   gain)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcSetGain Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       (chanId <= AUD_ADC_CH_ALL) &&
           (gain <= 100) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcSetGain : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_aud_pcm186xSetVolume(aud_gAdcI2cAddr[id], (Uint8)gain,
									 (Uint8)chanId);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcSetGain : ADC Access for Gain Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures ADC analog input selection for left channel
 *
 *  Default input selection of ADC left channels can be modified using
 *  this function.
 *
 *  Default input selection for ADC left channels is listed below
 *    CH1 LEFT  - VINL1
 *    CH2 LEFT  - VINL2
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          AUD_ADC_CH1_LEFT - Input selection for channel 1 left
 *                          AUD_ADC_CH2_LEFT - Input selection for channel 2 left
 *                          AUD_ADC_CH_ALL - Input selection for channel 1 & 2 left
 *
 *  \param inputMux   [IN]  Input mux configuration
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcSetLeftInputMux(AudAdcDevId        devId,
                                                AudAdcChanId       chanId,
                                                AudAdcLeftInputMux inputMux)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcSetLeftInputMux Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       ((chanId == AUD_ADC_CH1_LEFT) || (chanId == AUD_ADC_CH2_LEFT) ||
	        (chanId == AUD_ADC_CH_ALL)) &&
           ((inputMux <= AUD_ADC_INL_DIFF_VIN1P_VIN1M_VIN4P_VIN4M)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcSetLeftInputMux : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			if(chanId == AUD_ADC_CH_ALL)
			{
				retVal = aud_aud_pcm186xInputSel(aud_gAdcI2cAddr[id], (Uint8)AUD_ADC_CH1_LEFT,
										 (Uint8)inputMux);
				if(retVal)
				{
					IFPRINT(aud_write("aud_AudioAdcSetLeftInputMux : ADC Access for Input Selection Failed \n"));
					aud_errno = AUD_ERRNO_AUDIO_CFG;
					return (Aud_EFAIL);
				}

				retVal = aud_aud_pcm186xInputSel(aud_gAdcI2cAddr[id], (Uint8)AUD_ADC_CH2_LEFT,
										 (Uint8)inputMux);
			}
			else
			{
				retVal = aud_aud_pcm186xInputSel(aud_gAdcI2cAddr[id], (Uint8)chanId,
										 (Uint8)inputMux);
			}

			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcSetLeftInputMux : ADC Access for Input Selection Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures ADC analog input selection for right channel
 *
 *  Default input selection of ADC right channels can be modified using
 *  this function
 *
 *  Default input selection for ADC right channels is shown below
 *    CH1 RIGHT - VINR1
 *    CH2_RIGHT - VINR2
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                         AUD_ADC_CH1_RIGHT - Input selection for channel 1 right
 *                         AUD_ADC_CH2_RIGHT - Input selection for channel 2 right
 *                         AUD_ADC_CH_ALL - Input selection for channel 1 & 2 right
 *
 *  \param inputMux   [IN]  Input mux configuration
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcSetRightInputMux(AudAdcDevId         devId,
                                                 AudAdcChanId        chanId,
                                                 AudAdcRightInputMux inputMux)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcSetRightInputMux Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       ((chanId == AUD_ADC_CH1_RIGHT) || (chanId == AUD_ADC_CH2_RIGHT) ||
	        (chanId == AUD_ADC_CH_ALL)) &&
           ((inputMux <= AUD_ADC_INR_DIFF_VIN2P_VIN2M_VIN3P_VIN3M)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcSetRightInputMux : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			if(chanId == AUD_ADC_CH_ALL)
			{
				retVal = aud_aud_pcm186xInputSel(aud_gAdcI2cAddr[id], (Uint8)AUD_ADC_CH1_RIGHT,
										 (Uint8)inputMux);
				if(retVal)
				{
					IFPRINT(aud_write("aud_AudioAdcSetRightInputMux : ADC Access for Input Selection Failed \n"));
					aud_errno = AUD_ERRNO_AUDIO_CFG;
					return (Aud_EFAIL);
				}

				retVal = aud_aud_pcm186xInputSel(aud_gAdcI2cAddr[id], (Uint8)AUD_ADC_CH2_RIGHT,
										 (Uint8)inputMux);
			}
			else
			{
				retVal = aud_aud_pcm186xInputSel(aud_gAdcI2cAddr[id], (Uint8)chanId,
										 (Uint8)inputMux);
			}

			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcSetRightInputMux : ADC Access for Input Selection Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    ADC audio interface data configuration
 *
 *  This function configures serial audio interface data format and
 *  receive PCM word length for ADC
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param wLen       [IN]  ADC data word length
 *
 *  \param format     [IN]  Audio data format
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcDataConfig(AudAdcDevId      devId,
                                           AudAdcRxWordLen  wLen,
                                           AudAdcDataFormat format)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcDataConfig Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       ((wLen >= AUD_ADC_RX_WLEN_24BIT) && (wLen <= AUD_ADC_RX_WLEN_16BIT)) &&
           ((format <= AUD_ADC_DATA_FORMAT_TDM_DSP)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcDataConfig : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xDataConfig(aud_gAdcI2cAddr[id], (Uint8)format,
			                           (Uint8)wLen);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcDataConfig : ADC Access for Data Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Enables/Disables ADC channel mute
 *
 *  This function configures mute functionality of each ADC channel
 *
 *  \param devId      [IN]  Device ID of ADC HW instance
 *                          Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                          for all the ADC devices available.
 *
 *  \param chanId     [IN]  Internal ADC channel Id
 *                          Use AUD_ADC_CH_ALL to apply mute configuration for
 *                          all the ADC channels
 *
 *  \param muteEnable [IN]  Flag to configure mute
 *                          1 - Mute ADC channel
 *                          0 - Unmute ADC channel
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcMuteCtrl(AudAdcDevId  devId,
                                         AudAdcChanId chanId,
                                         uint8_t   muteEnable)
{
	AUD_ADC_RET  retVal;
	Uint8    chan;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcMuteCtrl Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       (chanId <= AUD_ADC_CH_ALL) &&
           ((muteEnable == 0) || (muteEnable == 1)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcMuteCtrl : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	if(chanId == AUD_ADC_CH_ALL)
	{
		chan = 0xF;
	}
	else
	{
		chan = (1 << chanId);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xMuteChannel(aud_gAdcI2cAddr[id], chan,
			                           (Uint8)muteEnable);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcMuteCtrl : ADC Access for Mute Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures ADC MIC bias
 *
 *  This function enables/disables MIC bias for analog MIC input
 *
 *  \param devId         [IN]  Device ID of ADC HW instance
 *                             Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                             for all the ADC devices available.
 *
 *  \param micBiasEnable [IN]  Mic Bias enable flag
 *                             1 - Enable MIC Bias
 *                             0 - Disable MIC Bias
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcMicBiasCtrl(AudAdcDevId devId,
                                            uint8_t  micBiasEnable)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcMicBiasCtrl Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
           ((micBiasEnable == 0) || (micBiasEnable == 1)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcMicBiasCtrl : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xMicBiasCtrl(aud_gAdcI2cAddr[id], (Uint8)micBiasEnable);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcMicBiasCtrl : ADC Access for Mic Bias Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures ADC power state
 *
 *  This function enables/disables different power modes supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param powState    [IN]  ADC power state to configure
 *
 *  \param stateEnable [IN]  Power state enable flag
 *                           1 - Enables the power state
 *                           0 - Disables the power state
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcConfigPowState(AudAdcDevId      devId,
                                               AudAdcPowerState powState,
                                               uint8_t       stateEnable)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcConfigPowState Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       (powState <= AUD_ADC_POWER_STATE_POWERDOWN) &&
           ((stateEnable == 0) || (stateEnable == 1)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcConfigPowState : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xConfigPowState(aud_gAdcI2cAddr[id], (Uint8)powState,
			                               (Uint8)stateEnable);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcConfigPowState : ADC Access for Power State Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures ADC interrupts
 *
 *  This function enables/disables different interrupts supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param intId       [IN]  Interrupt Id to configure
 *                           Use to 'AUD_ADC_INTR_ALL' to configure all the
 *                           interrupts together
 *
 *  \param intEnable   [IN]  Interrupt enable flag
 *                           1 - Enables the interrupt
 *                           0 - Disables the interrupt
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcConfigIntr(AudAdcDevId devId,
                                           AudAdcIntr  intId,
                                           uint8_t  intEnable)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcConfigIntr Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       (intId <= AUD_ADC_INTR_ALL) &&
           ((intEnable == 0) || (intEnable == 1)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcConfigIntr : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xSetIntr(aud_gAdcI2cAddr[id], (Uint8)intId,
			                        (Uint8)intEnable);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcConfigIntr : ADC Access for Intr Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Reads ADC interrupt status
 *
 *  This function reads the status of different interrupts supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *
 *  \param intId       [IN]  Interrupt Id to read the status
 *                           Use to 'AUD_ADC_INTR_ALL' to read status of all the
 *                           interrupts together
 *
 *  \return    Interrupt status
 *             1 - Interrupt occurred
 *             0 - No interrupt occurred
 *          0xFF - Error in reading interrupt
 */
uint8_t aud_AudioAdcGetIntrStatus(AudAdcDevId devId,
                                      AudAdcIntr  intId)
{
	Uint8  retVal;

	IFPRINT(aud_write("aud_AudioAdcGetIntrStatus Called \n"));

	if( !( ((devId <= AUD_ADC_DEVICE_1)) &&
	       (intId <= AUD_ADC_INTR_ALL) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcGetIntrStatus : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (1);
	}

	retVal = aud_pcm186xGetIntrStatus(aud_gAdcI2cAddr[devId]);
	if(retVal == 0xFF)
	{
		IFPRINT(aud_write("aud_AudioAdcGetIntrStatus : ADC Access for Intr Status Failed \n"));
		aud_errno = AUD_ERRNO_AUDIO_STATUS;
		return ((uint8_t)retVal);
	}

	if(intId != AUD_ADC_INTR_ALL)
	{
		retVal = (retVal >> intId) & 0x1;
	}

    return (retVal);
}

/**
 *  \brief    Reads ADC status bits
 *
 *  This function reads the value of different status functions supported
 *  by ADC module (excluding interrupts).
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *
 *  \param status      [IN]  Status function of which status to be read
 *
 *  \return    Value of status function or 0xFF in case of error
 *
 */
uint8_t aud_AudioAdcGetStatus(AudAdcDevId  devId,
                                  AudAdcStatus status)
{
	Uint8  retVal;

	IFPRINT(aud_write("aud_AudioAdcGetStatus Called \n"));

	if( !( ((devId <= AUD_ADC_DEVICE_1)) &&
	       (status <= AUD_ADC_STATUS_LDO) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcGetStatus : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (1);
	}

	switch(status)
	{
		/* Current Power State of the device
		 *        0x0 - Power Down
		 *        0x1 - Wait clock stable
		 *        0x2 - Release reset
		 *        0x3 - Stand-by
		 *        0x4 - Fade IN
		 *        0x5 - Fade OUT
		 *        0x9 - Sleep
         *        0xF - Run
		 */
		case AUD_ADC_STATUS_POWER_STATE:
			retVal = aud_pcm186xGetPowerStateStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* Current Sampling Frequency
		 *       0x0 - Out of range (Low) or LRCK Halt
		 *       0x1 - 8kHz
		 *       0x2 - 16kHz
		 *       0x3 - 32-48kHz
		 *       0x4 - 88.2-96kHz
		 *       0x5 - 176.4-192kHz
		 *       0x6 - Out of range (High)
         *       0x7 - Invalid Fs
		 */
		case AUD_ADC_STATUS_SAMPLING_FREQ:
			retVal = aud_pcm186xGetSampleFreqStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* Current receiving BCK ratio
		 *        0x0 - Out of range (L) or BCK Halt
		 *        0x1 - 32
		 *        0x2 - 48
		 *        0x3 - 64
		 *        0x4 - 256
		 *        0x6 - Out of range (High)
         *        0x7 - Invalid BCK ratio or LRCK Halt
		 */
		case AUD_ADC_STATUS_BCK_RATIO:
			retVal = aud_pcm186xGetBckRatioStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* Current SCK Ratio
		 *        0x0 - Out of range (L) or SCK Halt
		 *        0x1 - 128
		 *        0x2 - 256
		 *        0x3 - 384
		 *        0x4 - 512
		 *        0x5 - 768
		 *        0x6 - Out of range (High)
		 *        0x7 - Invalid SCK ratio or LRCK Halt
		 */
		case AUD_ADC_STATUS_SCK_RATIO:
			retVal = aud_pcm186xGetSckRatioStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* LRCK Halt Status
		 *     0 - No Error
         *     1 - Halt
		 */
		case AUD_ADC_STATUS_LRCK_HALT:
			retVal = aud_pcm186xGetLrckHltStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* BCK Halt Status
		 *     0 - No Error
         *     1 - Halt
		 */
		case AUD_ADC_STATUS_BCK_HALT:
			retVal = aud_pcm186xGetBckHltStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* SCK Halt Status
		 *     0 - No Error
         *     1 - Halt
		 */
		case AUD_ADC_STATUS_SCK_HALT:
			retVal = aud_pcm186xGetSckHltStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* LRCK Error Status
		 *     0 - No Error
		 *     1 - Error
	     */
		case AUD_ADC_STATUS_LRCK_ERR:
			retVal = aud_pcm186xGetLrckErrStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* BCK Error Status
		 *     0 - No Error
		 *     1 - Error
	     */
		case AUD_ADC_STATUS_BCK_ERR:
			retVal = aud_pcm186xGetBckErrStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* SCK Error Status
		 *     0 - No Error
		 *     1 - Error
	     */
		case AUD_ADC_STATUS_SCK_ERR:
			retVal = aud_pcm186xGetSckErrStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* DVDD Status
		 *   0 - Bad/Missing
         *   1 - Good
         */
		case AUD_ADC_STATUS_DVDD:
			retVal = aud_pcm186xGetDvddStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* AVDD Status
		 *   0 - Bad/Missing
         *   1 - Good
         */
		case AUD_ADC_STATUS_AVDD:
			retVal = aud_pcm186xGetAvddStatus(aud_gAdcI2cAddr[devId]);
			break;

		/* Digital LDO Status
		 *   0 - Bad/Missing
         *   1 - Good
         */
		case AUD_ADC_STATUS_LDO:
			retVal = aud_pcm186xGetLdoStatus(aud_gAdcI2cAddr[devId]);
			break;

		default:
			retVal = 0xFF;
			break;

	}

	if(retVal == 0xFF)
	{
		IFPRINT(aud_write("aud_AudioAdcGetStatus : ADC Access for Get Status Failed \n"));
		aud_errno = AUD_ERRNO_AUDIO_STATUS;
	}

	return ((uint8_t)retVal);
}

/**
 *  \brief    ADC DSP channel configuration control
 *
 *  This function configures the DSP module processing channels
 *  supported by ADC
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                           for all the ADC devices available.
 *
 *  \param chanCfg     [IN]  DSP channel configuration
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioAdcDspCtrl(AudAdcDevId      devId,
                                        AudAdcDspChanCfg chanCfg)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcDspCtrl Called \n"));

	if( !( (devId <= AUD_ADC_DEVICE_ALL) &&
	       ((chanCfg == AUD_ADC_DSP_PROC_4CHAN) ||
	        (chanCfg == AUD_ADC_DSP_PROC_2CHAN)) ) )
	{
		IFPRINT(aud_write("aud_AudioAdcDspCtrl : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xDspCtrl(aud_gAdcI2cAddr[id], (Uint8)chanCfg);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcDspCtrl : ADC Access for DSP Chan Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Programs ADC DSP coefficients
 *
 *  ADC module supports an internal DSP which performs additional audio
 *  processing operations like mixing, LPF/HPF etc.
 *  DSP coefficients can be programmed by this function.
 *
 *  \param devId        [IN]  Device ID of ADC HW instance
 *                            Use 'AUD_ADC_DEVICE_ALL' to apply the configuration
 *                            for all the ADC devices available.
 *
 *  \param coeffRegAddr [IN]  Address of DSP coefficient register
 *
 *  \param dspCoeff     [IN]  Value of DSP coefficient
 *                            Lower 24 bits are written to DSP coeff register
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioAdcProgDspCoeff(AudAdcDevId      devId,
                                             uint8_t       coeffRegAddr,
                                             uint32_t      dspCoeff)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcProgDspCoeff Called \n"));

	if(!(devId <= AUD_ADC_DEVICE_ALL))
	{
		IFPRINT(aud_write("aud_AudioAdcProgDspCoeff : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xProgDspCoeff(aud_gAdcI2cAddr[id], (Uint8)coeffRegAddr,
			                             (Uint32)dspCoeff);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcProgDspCoeff : ADC DSP Coefficient Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Displays ADC register programmed values
 *
 *  This function is provided for debug purpose to read the value
 *  of ADC registers. Values read from the ADC registers will be displayed
 *  in CCS output window or serial terminal based on the system level
 *  configuration for debug messages.
 *
 *  \param devId       [IN]  Device ID of ADC HW instance
 *                           Use 'AUD_ADC_DEVICE_ALL' to read the register values
 *                           for all the ADC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioAdcGetRegDump(AudAdcDevId devId)
{
	AUD_ADC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioAdcGetRegDump Called \n"));

	if(!(devId <= AUD_ADC_DEVICE_ALL))
	{
		IFPRINT(aud_write("aud_AudioAdcGetRegDump : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_ADC_DEVICE_0; id < AUD_ADC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_ADC_DEVICE_ALL))
		{
			retVal = aud_pcm186xRegDump(aud_gAdcI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioAdcGetRegDump : ADC Reg Read Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_STATUS;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

#endif  /* #if (AUD_AUDIO_ADC) */


#if (AUD_AUDIO_DAC)

/**
 *  \brief    Initializes DAC module
 *
 *  This function configures the system level setup required for DAC
 *  operation and initializes the DAC module with default values.
 *  This function should be called before calling any other DAC functions.
 *
 *  After executing this function, DAC module should be ready for audio
 *  processing with default configuration. Default DAC configurations
 *  can be changed using the other DAC APIs if required.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to initialize
 *                            all the DAC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacInit(AudDacDevId devId)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacInit Called \n"));

	if(!(devId <= AUD_DAC_DEVICE_ALL))
	{
		IFPRINT(aud_write("aud_AudioDacInit : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xDacInit(aud_gDacI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacInit : DAC Initialization Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_INIT;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Resets DAC module
 *
 *  Resetting the DAC module restarts the re-synchronization between
 *  system clock and sampling clock, and DAC operation.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to reset
 *                            all the DAC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacReset(AudDacDevId devId)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacReset Called \n"));

    /* Check input parameters */
	if(!(devId <= AUD_DAC_DEVICE_ALL))
	{
		IFPRINT(aud_write("aud_AudioDacReset : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			IFPRINT(aud_write("aud_AudioDacReset : DAC Reset Failed \n"));
			retVal = aud_pcm169xReset(aud_gDacI2cAddr[id]);
			if(retVal)
			{
				aud_errno = AUD_ERRNO_AUDIO;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures DAC Analog mute control
 *
 *  DAC module supports AMUTE functionality which causes the DAC output
 *  to cut-off from the digital input upon the occurrence of any events
 *  which are configured by AMUTE control.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to apply the configuration
 *                            for all the DAC devices available.
 *
 *  \param muteCtrl     [IN]  Analog mute control event
 *
 *  \param muteEnable   [IN]  Flag to configure AMUTE for given control event
 *                            1 - Enable AMUTE control
 *                            0 - Disable AMUTE control
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacAmuteCtrl(AudDacDevId     devId,
                                          AudDacAmuteCtrl muteCtrl,
                                          uint8_t      muteEnable)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacAmuteCtrl Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (muteCtrl <= AUD_DAC_AMUTE_CTRL_DAC_DISABLE_CMD) &&
	       ((muteEnable == 0) || (muteEnable == 1)) ) )
	{
		IFPRINT(aud_write("aud_AudioDacAmuteCtrl : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			if(muteEnable == 1)
			{
                retVal = aud_pcm169xEnableAmute(aud_gDacI2cAddr[id],
                                            (Uint8)(1 << muteCtrl));
			}
			else
			{
				retVal = aud_pcm169xDisableAmute(aud_gDacI2cAddr[id],
				                             (Uint8)(1 << muteCtrl));
			}

			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacAmuteCtrl : DAC Access for AMUTE Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures DAC Audio sampling mode
 *
 *  By default, DAC module sampling mode is configured for auto mode.
 *  In Auto mode, the sampling mode is automatically set according to multiples
 *  between the system clock and sampling clock. Single rate for 512 fS, 768 fS,
 *  and 1152 fS, dual rate for 256 fS or 384 fS, and quad rate for 128 fS
 *  and 192 fS. Setting the sampling mode is required only if auto mode
 *  configurations are not suitable for the application.
 *
 *  \param devId        [IN]  Device ID of DAC HW instance
 *                            Use 'DAC_DEVICE_ALL' to apply the configuration
 *                            for all the DAC devices available.
 *
 *  \param samplingMode [IN]  DAC audio sampling mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetSamplingMode(AudDacDevId        devId,
                                                AudDacSamplingMode samplingMode)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSetSamplingMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (samplingMode <= AUD_DAC_SAMPLING_MODE_QUAD_RATE) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetSamplingMode : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xSetSamplingMode(aud_gDacI2cAddr[id],
			                                (Uint8)samplingMode);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetSamplingMode : DAC Access for Sampling Mode Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures DAC Audio interface data format
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param dataFormat [IN]  DAC audio data format
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetDataFormat(AudDacDevId      devId,
                                              AudDacDataFormat dataFormat)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSetDataFormat Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (dataFormat <= AUD_DAC_DATA_FORMAT_24BIT_HS_LEFTJ_TDM) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetDataFormat : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xDataConfig(aud_gDacI2cAddr[id], (Uint8)dataFormat);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetDataFormat : DAC Access for Data Format Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures DAC operation mode
 *
 *  This function configures a particular DAC channel pair to be operating
 *  normal or disabled.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanPair   [IN]  Internal DAC channel pair
 *                          Use AUD_DAC_CHANP_1_2 to AUD_DAC_CHANP_7_8 for
 *                          individual DAC channel pair configuration
 *                          Use AUD_DAC_CHANP_ALL to set operation mode
 *                          for all DAC channels
 *
 *  \param opMode     [IN]  DAC operation mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetOpMode(AudDacDevId    devId,
                                          AudDacChanPair chanPair,
                                          AudDacOpMode   opMode)
{
	AUD_DAC_RET  retVal;
	Uint8    modeCtrl;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSetOpMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (chanPair <= AUD_DAC_CHANP_ALL) &&
	       ((opMode == AUD_DAC_OPMODE_NORMAL) ||
	        (opMode == AUD_DAC_OPMODE_DISABLED)) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetOpMode : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	if(chanPair == AUD_DAC_CHANP_ALL)
	{
		modeCtrl = 0xF;
	}
	else
	{
		modeCtrl = (1 << chanPair);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			if(opMode == AUD_DAC_OPMODE_DISABLED)
			{
                retVal = aud_pcm169xDisableDacOpeartion(aud_gDacI2cAddr[id], modeCtrl);
			}
			else
			{
				retVal = aud_pcm169xEnableDacOpeartion(aud_gDacI2cAddr[id], modeCtrl);
			}

			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetOpMode : DAC Access for Opmode Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures DAC filter roll-off
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanPair   [IN]  Internal DAC channel pair
 *                          Use AUD_DAC_CHANP_1_2 to AUD_DAC_CHANP_7_8 for
 *                          individual DAC channel pair configuration
 *                          Use AUD_DAC_CHANP_ALL to set filter roll-off
 *                          for all DAC channels
 *
 *  \param rolloff    [IN]  Roll-off configuration
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetFilterRolloff(AudDacDevId         devId,
                                                 AudDacChanPair      chanPair,
                                                 AudDacFilterRolloff rolloff)
{
	AUD_DAC_RET  retVal;
	Uint8    chanId;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSetFilterRolloff Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (chanPair <= AUD_DAC_CHANP_ALL) &&
	       (rolloff <= AUD_DAC_FILTER_SLOW_ROLLOFF) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetFilterRolloff : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	if(chanPair == AUD_DAC_CHANP_ALL)
	{
		chanId = 0xF;
	}
	else
	{
		chanId = (1 << chanPair);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xSetFilterRolloff(aud_gDacI2cAddr[id], chanId,
			                                 (Uint8)rolloff);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetFilterRolloff : DAC Access for Filter Roll-off Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures phase of the DAC analog signal outputs
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanId     [IN]  Internal DAC channel Id
 *                          Use AUD_DAC_CHAN_1 to AUD_DAC_CHAN_8 for individual
 *                          DAC channel configuration
 *                          Use AUD_DAC_CHAN_ALL to set output phase for all
 *                          DAC channels
 *
 *  \param outPhase   [IN]  Output phase selection
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetOutputPhase(AudDacDevId       devId,
                                               AudDacChanId      chanId,
                                               AudDacOutputPhase outPhase)
{
	AUD_DAC_RET  retVal;
	Uint8    chan;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSetOutputPhase Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (chanId <= AUD_DAC_CHAN_ALL) &&
	       (outPhase <= AUD_DAC_OUTPUT_PHASE_INVERTED) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetOutputPhase : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	if(chanId == AUD_DAC_CHAN_ALL)
	{
		chan = 0xFF;
	}
	else
	{
		chan = (1 << chanId);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xSetOutputPhase(aud_gDacI2cAddr[id], chan,
			                               (Uint8)outPhase);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetOutputPhase : DAC Access for Output Phase Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Soft mute function control
 *
 *  The Soft mute function allows mute/unmute of DAC output in gradual steps.
 *  This configuration reduces pop and zipper noise during muting of the
 *  DAC output.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \param chanId     [IN]  Internal DAC channel Id
 *                          Use AUD_DAC_CHAN_1 to AUD_DAC_CHAN_8 for individual
 *                          DAC channel configuration
 *                          Use AUD_DAC_CHAN_ALL to mute/unmute all DAC
 *                          channels
 *
 *  \param muteEnable [IN]  Mute enable flag
 *                          0 for unmute and 1 for mute
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSoftMuteCtrl(AudDacDevId   devId,
                                             AudDacChanId  chanId,
                                             uint8_t    muteEnable)
{
	AUD_DAC_RET  retVal;
	Uint8    chan;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSoftMuteCtrl Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (chanId <= AUD_DAC_CHAN_ALL) &&
	       ((muteEnable == 0) || (muteEnable == 1)) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSoftMuteCtrl : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	if(chanId == AUD_DAC_CHAN_ALL)
	{
		chan = 0xFF;
	}
	else
	{
		chan = (1 << chanId);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xSoftMuteCtrl(aud_gDacI2cAddr[id], chan, muteEnable);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSoftMuteCtrl : DAC Access for Soft Mute Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Sets attenuation mode
 *
 *  DAC module supports two types of volume/attenuation dB range
 *  which can be changed by setting attenuation mode. Volume range and fine
 *  tuning will change based on the attenuation mode.
 *
 *  \param devId    [IN]  Device ID of DAC HW instance
 *                        Use 'DAC_DEVICE_ALL' to apply the configuration
 *                        for all the DAC devices available.
 *
 *  \param attnMode [IN]  Attenuation mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetAttnMode(AudDacDevId    devId,
                                            AudDacAttnMode attnMode)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSetAttnMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (attnMode <= AUD_DAC_ATTENUATION_WIDE_RANGE) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetAttnMode : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xSetAttnMode(aud_gDacI2cAddr[id], (Uint8)attnMode);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetAttnMode : DAC Access for Attenuation Mode Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Function to control digital de-emphasis functionality
 *
 *  Disables/Enables the various sampling frequencies of the digital
 *  de-emphasis function.
 *
 *  \param devId     [IN]  Device ID of DAC HW instance
 *                         Use 'DAC_DEVICE_ALL' to apply the configuration
 *                         for all the DAC devices available.
 *
 *  \param deempCtrl [IN]  De-emphasis control options
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacDeempCtrl(AudDacDevId     devId,
                                          AudDacDeempCtrl deempCtrl)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacDeempCtrl Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (deempCtrl <= AUD_DAC_DEEMP_32KHZ) ) )
	{
		IFPRINT(aud_write("aud_AudioDacDeempCtrl : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xDeempCtrl(aud_gDacI2cAddr[id], (Uint8)deempCtrl);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacDeempCtrl : DAC Access for De-emphasis Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures DAC volume/attenuation
 *
 *  Range of the volume is exposed as percentage by this API. Volume
 *  is indicated as percentage of maximum value ranging from 0 to 100.
 *  0 to mute the volume and 100 to set maximum volume
 *
 *  DAC module supports two types of volume/attenuation dB range
 *  which can be changed using aud_AudioDacSetAttnMode().
 *  Volume range and fine tuning will change based on the attenuation mode.
 *
 *  \param devId  [IN]  Device ID of DAC HW instance
 *                      Use 'DAC_DEVICE_ALL' to apply the configuration
 *                      for all the DAC devices available.
 *
 *  \param chanId [IN]  Internal DAC channel Id
 *                      Use AUD_DAC_CHAN_1 to AUD_DAC_CHAN_8 for individual
 *                      DAC channel configuration
 *                      Use AUD_DAC_CHAN_ALL to mute/unmute all DAC
 *                      channels
 *
 *  \param volume [IN]  Volume in percentage; 0 to 100
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetVolume(AudDacDevId devId,
                                          AudDacChanId chanId,
                                          uint8_t  volume)
{
	AUD_DAC_RET  retVal;
	Uint8    id;
	Uint8    chan;

	IFPRINT(aud_write("aud_AudioDacSetVolume Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       (chanId <= AUD_DAC_CHAN_ALL) &&
	       (volume <= 100) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetVolume : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	if(chanId == AUD_DAC_CHAN_ALL)
	{
		chan = 0xF;
	}
	else
	{
		chan = chanId;
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xSetVolume(aud_gDacI2cAddr[id], (Uint8)volume, chan);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetVolume : DAC Access for Volume Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Configures DAC power-save mode
 *
 *  \param devId     [IN]  Device ID of DAC HW instance
 *                         Use 'DAC_DEVICE_ALL' to apply the configuration
 *                         for all the DAC devices available.
 *
 *  \param PowerMode [IN]  Power-save mode control
 *                         0 - Enable power-save mode
 *                         1 - Disable power-save mode
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacSetPowerMode(AudDacDevId devId,
                                             uint8_t  PowerMode)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacSetPowerMode Called \n"));

    /* Check input parameters */
	if( !( (devId <= AUD_DAC_DEVICE_ALL) &&
	       ((PowerMode == 0) || (PowerMode == 1)) ) )
	{
		IFPRINT(aud_write("aud_AudioDacSetPowerMode : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xSetPowerMode(aud_gDacI2cAddr[id], PowerMode);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacSetPowerMode : DAC Access for Power-save Mode Config Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_CFG;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}

/**
 *  \brief    Displays DAC register programmed values
 *
 *  This function is provided for debug purpose to read the value
 *  of DAC registers. Values read from the DAC registers will be displayed
 *  in CCS output window or serial terminal based on the system level
 *  configuration for debug messages.
 *
 *  \param devId      [IN]  Device ID of DAC HW instance
 *                          Use 'DAC_DEVICE_ALL' to apply the configuration
 *                          for all the DAC devices available.
 *
 *  \return    Aud_EOK on Success or error code
 */
Aud_STATUS aud_AudioDacGetRegDump(AudDacDevId devId)
{
	AUD_DAC_RET  retVal;
	Uint8    id;

	IFPRINT(aud_write("aud_AudioDacGetRegDump Called \n"));

	if(!(devId <= AUD_DAC_DEVICE_ALL))
	{
		IFPRINT(aud_write("aud_AudioDacGetRegDump : Invalid Inputs\n"));
		aud_errno = AUD_ERRNO_INVALID_ARGUMENT;
		return (Aud_EINVALID);
	}

	for (id = AUD_DAC_DEVICE_0; id < AUD_DAC_DEVICE_ALL; id++)
	{
		if((id == devId) || (devId == AUD_DAC_DEVICE_ALL))
		{
			retVal = aud_pcm169xRegDump(aud_gDacI2cAddr[id]);
			if(retVal)
			{
				IFPRINT(aud_write("aud_AudioDacGetRegDump : DAC Access for Reg Dump Failed \n"));
				aud_errno = AUD_ERRNO_AUDIO_STATUS;
				return (Aud_EFAIL);
			}
		}
	}

    return (Aud_EOK);
}
#endif  /* #if (AUD_AUDIO_DAC) */


#if (AUD_AUDIO_DIR)

/**
 *  \brief    Initializes DIR module
 *
 *  Configures GPIOs and other settings required for DIR module operation.
 *  This function should be called before calling any other DIR functions.
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioDirInit(void)
{
	IFPRINT(aud_write("aud_AudioDirInit Called \n"));

	/* Configure each SOC_GPIO pin connected to DIR status lines
	   as GPIO and input */

	/* DIR RST signal - GPIO 1, pin 9 */
	aud_pinMuxSetMode(AUD_AUDIO_DIR_RST_PADCFG,
	              AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_1, AUD_AUDIO_DIR_RST_GPIO, AUD_GPIO_OUT);
	aud_gpioSetOutput(AUD_GPIO_PORT_1, AUD_AUDIO_DIR_RST_GPIO);

	/* DIR AUDIO signal - GPIO 0, pin 134 */
	aud_pinMuxSetMode(AUD_AUDIO_DIR_AUDIO_PADCFG,
	              AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_AUDIO_GPIO, AUD_GPIO_IN);

	/* DIR EMPH signal - GPIO 0, pin 135 */
	aud_pinMuxSetMode(AUD_AUDIO_DIR_EMPH_PADCFG,
	              AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_EMPH_GPIO, AUD_GPIO_IN);

	/* DIR ERROR signal - GPIO 0, pin 136 */
	aud_pinMuxSetMode(AUD_AUDIO_DIR_ERR_PADCFG,
	              AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_ERR_GPIO, AUD_GPIO_IN);

	/* DIR CLKST signal - GPIO 0, pin 133 */
	aud_pinMuxSetMode(AUD_AUDIO_DIR_CLKST_PADCFG,
	              AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_CLKST_GPIO, AUD_GPIO_IN);

	/* DIR FSOUT0 signal - GPIO 0, pin 124 */
	aud_pinMuxSetMode(AUD_AUDIO_DIR_FSOUT0_PADCFG,
	              AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_FSOUT0_GPIO, AUD_GPIO_IN);

	/* DIR FSOUT1 signal - GPIO 0, pin 15 */
	aud_pinMuxSetMode(AUD_AUDIO_DIR_FSOUT1_PADCFG,
	              AUD_PADCONFIG_MUX_MODE_QUATERNARY);
	aud_gpioSetDirection(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_FSOUT1_GPIO, AUD_GPIO_IN);

#if 0
    /* Reset DIR */
	aud_gpioClearOutput(AUD_GPIO_PORT_1, AUD_AUDIO_DIR_RST_GPIO);
	/* DIR Reset signal should be low for at least 100 ns.
	   Adding a delay of 1usec */
	aud_delay(1);
	aud_gpioSetOutput(AUD_GPIO_PORT_1, AUD_AUDIO_DIR_RST_GPIO);
#endif

    return (Aud_EOK);
}

/**
 *  \brief    Resets DIR module
 *
 *  \return    Aud_EOK on Success or error code
 *
 */
Aud_STATUS aud_AudioDirReset(void)
{
	IFPRINT(aud_write("aud_AudioDirReset Called \n"));

	aud_gpioClearOutput(AUD_GPIO_PORT_1, AUD_AUDIO_DIR_RST_GPIO);
	/* DIR Reset signal should be low for at least 100 ns.
	   Adding a delay of 1usec */
	aud_delay(1);
	aud_gpioSetOutput(AUD_GPIO_PORT_1, AUD_AUDIO_DIR_RST_GPIO);

    return(Aud_EOK);
}

/**
 *  \brief    Reads AUDIO output status value of the DIR
 *
 *  DIR AUDIO output pin gives the audio sample word information
 *  of the channel-status data bit 1
 *
 *  \return AUDIO pin output with below possible values
 *  \n      0 - Audio sample word represents linear PCM samples
 *  \n      1 - Audio sample word is used for other purposes
 */
int8_t aud_AudioDirGetAudioStatus(void)
{
	int8_t audio;

	IFPRINT(aud_write("aud_AudioDirGetAudioStatus Called \n"));

	audio = aud_gpioReadInput(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_AUDIO_GPIO);

    return(audio);
}

/**
 *  \brief    Reads EMPH output status value of the DIR
 *
 *  DIR EMPH output pin gives the emphasis information of the
 *  channel-status data bit 3.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Two audio channels without pre-emphasis
 *  \n      1 - Two audio channels with 50 ms / 15 ms pre-emphasis
 */
int8_t aud_AudioDirGetEmphStatus(void)
{
	int8_t emph;

	IFPRINT(aud_write("aud_AudioDirGetEmphStatus Called \n"));

	emph = aud_gpioReadInput(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_EMPH_GPIO);

    return(emph);
}

/**
 *  \brief    Reads ERROR pin status value of the DIR
 *
 *  DIR ERROR output pin gives the error state of data and parity errors.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Lock state of PLL and nondetection of parity error
 *  \n      1 - Unlock state of PLL or detection of parity error
 */
int8_t aud_AudioDirGetErrStatus(void)
{
	int8_t err;

	IFPRINT(aud_write("aud_AudioDirGetErrStatus Called \n"));

	err = aud_gpioReadInput(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_ERR_GPIO);

    return(err);
}

/**
 *  \brief    Reads CLKST pin status value of the DIR
 *
 *  DIR CLKST pin outputs the PLL status change between LOCK and UNLOCK.
 *  The CLKST output pulse depends only on the status change of the PLL.
 *
 *  \return EMPH pin output with below possible values
 *  \n      0 - Lock state of PLL and nondetection of parity error
 *  \n      1 - Unlock state of PLL or detection of parity error
 */
int8_t aud_AudioDirGetClkStatus(void)
{
	int8_t clk;

	IFPRINT(aud_write("aud_AudioDirGetClkStatus Called \n"));

	clk = aud_gpioReadInput(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_CLKST_GPIO);

    return(clk);
}

/**
 *  \brief    Reads FSOUT[1:0] output status value of the DIR
 *
 *  The DIR module calculates the actual sampling frequency of the
 *  biphase input signal and outputs its result through FSOUT[1:0] pins.
 *
 *  \return FSOUT pin output with below possible values
 *  \n      0 - Calculated Sampling Frequency Output is 43 kHz–45.2 kHz
 *  \n      1 - Calculated Sampling Frequency Output is 46.8 kHz–49.2 kHz
 *  \n      2 - Out of range or PLL unlocked
 *  \n      3 - Calculated Sampling Frequency Output is 31.2 kHz–32.8 kHz
 */
int8_t aud_AudioDirGetFsOut(void)
{
	int8_t fsout;

	IFPRINT(aud_write("aud_AudioDirGetFsOut Called \n"));

	fsout = aud_gpioReadInput(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_FSOUT1_GPIO);
	fsout <<= 1;
	fsout |= aud_gpioReadInput(AUD_GPIO_PORT_0, AUD_AUDIO_DIR_FSOUT0_GPIO);

    return(fsout);
}

#endif /* #if (AUD_AUDIO_DIR) */

#endif /* #if (AUD_AUDIO) */

/* Nothing past this point */
